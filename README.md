8 mai 2021:

Ce projet est celui du TIPE, mené en 2017.

J'aimerais reprendre le projet, le nettoyer et le rendre utilisable (commentaires (surtout docstring), séparation (propre) en fichiers...).

Le problème est de retrouver les infos.

Le fichier `Presentation.py` n'a pas d'erreurs. Le fichier `PasDeDifference.py` est daté du même jour et semble similaire mais avec des erreurs: la fonction tri_lin n'est pas définie (elle est définie dans `Fichier global.py`). Et le nombre de lignes n'est pas du tout le même, etc. Je pense qu'on a enlevé des commentaires qui étaient pour nous et ne devaient pas apparaître à la présentation. `$ diff` montre qu'en fait il n'y a pas tant de différence que ça...

C'est dur de savoir quel fichier est le plus récent et avec les bonnes fonctions. Les noms ont peut-être été changés, mais les parties états notamment sont un peu éparpillées entre les fichiers, et comme aucun n'en appelle un autre, on ne sait pas si les informations sont en double ou alors s'il faut aller les chercher.

---
* 31 mars 2022 *

This project is about site swap. This name refers directly to the operation of swapping landing sites of different throws a juggler performs. By metonymy, it is also the name given to the notation itself, and even sometimes to patterns described in this notation.

In the docs we will use the word "word" to define any finite sequence of numbers, defined in Python as a list or array, for example, [4, 3, 2] or [2, 10, 1]. This is the same definition as in theoretical computer science, the alphabet being [[1,h]] with h the maximum height allowed. Then, a word can be jugglable, or not.

The code in this project provides or will provide tools for:
- generating (in at least 4 different ways : brute force with permutation test; inverse permutation test; with site swaps from the fountain / cascade; with the graph of states) all possible patterns under given constraints (ie the set of jugglable words under these constraints). The different ways given are meant to be compared in terms of execution time.
- generating the graph of juggling states under constraints;
- finding transition throws between patterns in different states.

At first, only vanilla site swap is supported, but the long term goal is to include multiplex and synchronous throws and even multi-hand / passing.

# Organization

Old files (from TIPE) are in `Old` and new ones (supposedly well organized) in `New`.

Imagined architecture (closely follows the architecture of Présentation.py):
- one file for utils / tools
- one for generation with permutation test
- one for exchange of adjacent throws, aka site swap
- one for reverse-permutation test
- one for states without graphs at all
- one for graphs
- about difficulty and full shows: I'm still not convinced.

# TODO
** Currently **
- Replace variable names
- Split too long lines
- Understand and comment old code

** Short term **
- Switch to English
- Sort out all the mess in "Old" and put it organizedly in "New"
> This is being done from `old/Fichier global.py` to `new/generate.py`.
- Pythonize: docstring !!!
- Optimize / numpyze


** Long term **
- TypeHint
- Add tests

- Add features ?

    - offer how to fix a broken sequence (add 2 to this or that throw etc.)
    - multiplex (quite easy: for the permutation test, several balls are thrown at the same time but arrive at different times. And check if several balls arrive in hand just before several are thrown)
    - synchronous (different representation but same principles)
    - passing (I still have to read about it) : multi-hand notation ; prechac...

# DONE
- Make it a package so that it can be installed with `pip install -e`
- Find a transition between two patterns (especially of different state)
- Optimize pattern generation (vanilla site swap) with Polster's algorithm reversing the permutation test
