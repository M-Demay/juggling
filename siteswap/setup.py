#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Setup for the installation of this package.

@author: mathis
"""

import setuptools

setuptools.setup(
    name='siteswap', # name used when calling 'import ...'
    version='0.1.0',
    packages=setuptools.find_packages(exclude=["test"]),
    author='Mathis Demay',
    description=(
        "Operations on juggling words / sequences."
    ),
    install_requires=[
        "numpy",
    ]
)
