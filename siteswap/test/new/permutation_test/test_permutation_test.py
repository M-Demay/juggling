import unittest

import siteswap.new.permutation_test as pt
import numpy as np

class PermutationTestTestCase(unittest.TestCase):
    """
    Unittest class to test file new/permutation_test.py .
    """

    def test_incr_list_one(self):
        a = [0,0,0]
        self.assertEqual([0,0,1], pt.incr_list(a,1))

    def test_incr_list_shift_column(self):
        a = [0,5,5]
        self.assertEqual([1,0,0], pt.incr_list(a, 5))

    def test_incr_list_too_big_444(self):
        a = [4,4,4]
        with self.assertRaises(IndexError) as cm:
            pt.incr_list(a, 4)
        self.assertEqual(
            str(cm.exception),
            "[incr_list] Cannot increment list: maximal number reached."
        )

    def test_incr_list_too_big_777(self):
        a = [7,7,7]
        with self.assertRaises(IndexError) as cm:
            pt.incr_list(a, 7)
        self.assertEqual(
            str(cm.exception),
            "[incr_list] Cannot increment list: maximal number reached."
        )

    def test_gen_q33(self):
        res_33 = pt.gen_q(b_prime=3, period=3)
        self.assertTrue((res_33==np.array([
                                            [0, 0, 3],
                                            [0, 1, 2],
                                            [0, 2, 1],
                                            [0, 3, 0],
                                            [1, 0, 2],
                                            [1, 1, 1],
                                            [1, 2, 0],
                                            [2, 0, 1],
                                            [2, 1, 0],
                                            [3, 0, 0]
                                          ])
                        ).all()
        )

    def test_gen_q23(self):
        res_23 = pt.gen_q(2,3)
        self.assertTrue((res_23==np.array([
                                            [0, 0, 2],
                                            [0, 1, 1],
                                            [0, 2, 0],
                                            [1, 0, 1],
                                            [1, 1, 0],
                                            [2, 0, 0]
                                          ])
                        ).all()
        )

    def test_gen_q13(self):
        res_13 = pt.gen_q(1, 3)
        self.assertTrue((res_13==np.array([
                                            [0, 0, 1],
                                            [0, 1, 0],
                                            [1, 0, 0]
                                          ])
                        ).all()
        )

    def test_gen_q43(self):
        res_43 = np.array([
            [0, 0, 4],
            [0, 1, 3],
            [0, 2, 2],
            [0, 3, 1],
            [0, 4, 0],
            [1, 0, 3],
            [1, 1, 2],
            [1, 2, 1],
            [1, 3, 0],
            [2, 0, 2],
            [2, 1, 1],
            [2, 2, 0],
            [3, 0, 1],
            [3, 1, 0],
            [4, 0, 0]
        ])
        self.assertTrue((pt.gen_q(4,3)==res_43).all())

    def test_gen_q34(self):
        res = np.array([
            [0, 0, 0, 3],
            [0, 0, 1, 2],
            [0, 0, 2, 1],
            [0, 0, 3, 0],
            [0, 1, 0, 2],
            [0, 1, 1, 1],
            [0, 1, 2, 0],
            [0, 2, 0, 1],
            [0, 2, 1, 0],
            [0, 3, 0, 0],
            [1, 0, 0, 2],
            [1, 0, 1, 1],
            [1, 0, 2, 0],
            [1, 1, 0, 1],
            [1, 1, 1, 0],
            [1, 2, 0, 0],
            [2, 0, 0, 1],
            [2, 0, 1, 0],
            [2, 1, 0, 0],
            [3, 0, 0, 0]
        ])
        self.assertTrue((res==pt.gen_q(3,4)).all())


if __name__ == '__main__':
    unittest.main()