import unittest

from siteswap.new.states import NotThrowableError
import siteswap.new.states as sns


class StatesTestCase(unittest.TestCase):
    """
    Unittest class to test file new/states.py .
    """

    def test_get_state_51(self):
        # 3-ball, excited, have to duplicate
        state = sns.get_state_from_pattern([5,1])
        self.assertEqual(state, [1, 1, 0, 1])

    def test_get_state_5511(self):
        # Have to trim two extra 0s on top, ground state
        state = sns.get_state_from_pattern([5,5,1,1])
        self.assertEqual(state, [1, 1, 1])

    def test_get_state_450(self):
        # 3-ball, excited, have to duplicate, with a 0 throw
        state = sns.get_state_from_pattern([4,5,0])
        self.assertEqual(state, [1,1,0,1])

    def test_get_state_741(self):
        # 4-ball, excited, have to duplicate
        state = sns.get_state_from_pattern([7,4,1])
        self.assertEqual(state, [1,1,1,0,1])

    def test_can_throw_from_state_with_zero_ball_landing_false(self):
        self.assertFalse(sns.can_throw_from_state([0,1,1], 1))

    def test_can_throw_from_state_with_zero_ball_landing_true(self):
        self.assertTrue(sns.can_throw_from_state([0,1,1], 0))

    def test_can_throw_from_state_with_ball_landing_and_higher_true(self):
        self.assertTrue(sns.can_throw_from_state([1,0,0], 5))

    def test_can_throw_from_state_with_ball_landing_true(self):
        self.assertTrue(sns.can_throw_from_state([1,1,1], 3))

    def test_can_throw_from_state_with_ball_landing_false(self):
        self.assertFalse(sns.can_throw_from_state([1,1,1], 2))

    def test_throw_from_state_3_cascade(self):
        new_state = sns.throw_from_state([1,1,1], 3)
        self.assertEqual(new_state, [1,1,1])

    def test_throw_from_state_0_true(self):
        new_state = sns.throw_from_state([0,1,1],0)
        self.assertEqual(new_state, [1,1])

    def test_throw_from_state_0_false(self):
        self.assertRaises(NotThrowableError,
                          sns.throw_from_state,
                          [0,1,1], 5)

    def test_throw_from_state_higher(self):
        new_state = sns.throw_from_state([1,1,1],5)
        self.assertEqual(new_state, [1,1,0,0,1])

    def test_get_transition_with_states_3_balls(self):
        pattern1_container = [[4,5,0],
                              [3],
                              [6,0]
                             ]
        pattern2_container = [[3],
                              [4,5,0],
                              [7,1,3,1]
                             ]
        for k in range(len(pattern1_container)):
            pattern1, pattern2 = pattern1_container[k], pattern2_container[k]
            with self.subTest(i=k,
                msg="Testing pattern "+str(pattern1)+" to "+str(pattern2)):

                transition = sns.transition_with_states(pattern1, pattern2)
                aux_state = sns.get_state_from_pattern(pattern1)
                for throw in transition:
                    aux_state = sns.throw_from_state(aux_state, throw)
                self.assertEqual(aux_state,
                                 sns.get_state_from_pattern(pattern2))

if __name__ == '__main__':
    unittest.main()