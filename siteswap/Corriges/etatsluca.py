"""
De mémoire les rustines sont les transitions entre deux états.

Les états sont représentés comme des listes de booléens, le dernier élément
de la liste étant celui de hauteur maximale.

TODO : Pourquoi pas renvoyer les rustines dans les deux sens entre les deux etats.
    Voire rendre le script executable en lui-meme
    Cf https://github.com/JoshMermel/Juggle-Transition/tree/master
"""


def suivant_rustine(liste, max):
    """
    Génère la rustine suivante à tester.

    Exemple
    -------
    Avec max = 3
    0 1 2 00 01 02 10 11 000 ...

    Parameters
    ----------
    liste : list[int]
        Rustine soit séquence de lancers (pas forcément jonglable)

    max : int
        Hauteur maximale de lancer dans la rustine.
        TODO : Renommer en h_max pour clarte (car != longueur max)

    Returns
    -------
    list[bool]
        La rustine suivante.
    """
    n = len(liste)
    if n == 0:
        return [0]
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1):
        liste[-k] += 1
        return(liste)
    else:
        return ([0]*(n+1))

def lancer_sur(etats, h):
    """
    Génère l'état obtenu après avoir lancé une balle à la hauteur `h`
    à partir de l'état `etats` et le retourne (tout en marchant en place).

    Parameters
    ----------
    etats : list[bool]
        Etat jonglé : True s'il y a une balle, False sinon.
    h : int
        Hauteur du lancer à exécuter.

    Returns
    -------
    etats : list[bool]
        `etats` fourni en entrée, mis à jour après avoir lancé `h`.

    TODO : Pas propre ! Faire une classe d'états ? Ou juste mettre a jour ?
        Ou renvoyer. Ou laisser le choix.

    """
    etats = etats[1:] + [False]
    if h != 0:
        etats[h-1] = True
    return (etats)

def lancer_verifie(etats, h):
    """
    Tente de lancer une balle avec hauteur `h` a partir de l'etat `etats`.

    Parameters
    ----------
    etats : list[bool]
        Etat jonglé : True s'il y a une balle, False sinon.
    h : int
        Hauteur du lancer a executer.

    Returns
    -------
    True
    etats : list[bool]
        `etats` fourni en entrée, mis à jour après avoir lancé `h`.

    ---- OU ----
    False, False
        Si impossible de lancer `h` a partir de `etats`.

    Note
    ----
    Utile de renvoyer le booleen car utilise dans `forcing1`.
    """
    if h == 0:
        if etats[0]:
            # On tente de lancer un 0 alors qu'une balle tombe : impossible
            return False, False
        else:
            return True, (etats[1:] + [False])
    if (not etats[0]):
        # On tente de lancer une balle mais aucune n'arrive : impossible
        return False, False
    if etats[h]:
        # On tente de lancer une balle sur un niveau ou il y en a deja une :
        # impossible en siteswap vanille car alors
        # on devra rattraper deux balles en meme temps
        return False, False
    else:
        etats = etats[1:] + [False]
        etats[h-1] = True
        return (True, etats)

def forcing1(l1, l2, max):
    """
    Etablit une rustine de `l1` vers `l2`, en force brute :
    Essaie toutes les suites de lancers possibles de hauteur inferieure
    ou egale a `max` jusqu'a arriver a l'etat de `l2`.
    -- Heu, pas sur. A relire. Auxiliaire pour forcing2 ?

    TODO: Semble juste verifier qu'on peut faire la sequence de lancer puis l1;
        Rien ne garantit qu'on arrive dans l'etat de l2.

    Parameters
    ----------
    l1 : list[int]
        Premiere figure
    l2 : list[int]
        Deuxieme figure
    max : int
        hauteur max
        TODO: renommer en h_max pour clarte (car != longueur max)
            Et car max est un mot réservé !

    Returns
    -------
    bool : bool
        Indique la reussite.
        TODO: Changer nom : mot reserve !
        Ne pas enlever: utilise dans forcing2.
    rustine : list[int]
        Rustine ie transition de l1 vers l2.
    """
    etats = [False for k in range (0, max+1)]
    n1, n2 = len(l1), len(l2)
    # On met `etats` a l'etat de l1.
    # Justification du while : par exemple pour 6712 il faut faire au moins deux
    # boucles de ce while pour avoir le bon etat, sinon on n'a que 3 balles
    # sur 4. Donc on fait des boucles jusqu'a atteindre le nb de balles.
    nb_balles_n1 = int(sum(l1)/len(l1))
    while sum(etats) < nb_balles_n1 :
        for k in range (0, n1):
            etats = lancer_sur(etats, l1[k])

    taille_max = 3 # Arbitraire ! Longueur maximale de la rustine
    # TODO : Mettre en parametre, avec valeur par defaut si on veut.
    # Voire lever exception "taille_max atteinte"
    rustine = []
    bool = False
    # Semble indiquer la réussite, ie si on a trouve la bonne rustine ou non
    while (len(rustine) <= taille_max) and (not bool):
        # Peut ne pas s'arreter ?
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:] # Concatenation : on tente d'executer les lancers de rustine puis de l2 a la suite ?
        n3 = len(liste)
        # TODO: relire / tester pour exactitude
        k=0
        while bool and (k < n3):
            # On essaie d'effectuer tous les lancers de `liste`,
            # soit rustine puis l2
            if k == len(rustine):
                # On capture l'etat apres avoir lance la rustine a partir de l1
                apresrus = etats_test[:]
            bool, etats_test = lancer_verifie(etats_test, liste[k])
            k += 1
        if not bool:
            # Un lancer n'a pas pu etre fait : on tente la rustine suivante
            rustine = suivant_rustine(rustine, max)
        else:
            # On regarde si l'etat avant et apres l2 est le meme
            bool = (apresrus == etats_test)

    return(bool, rustine)


def forcing2(l1, l2, max):
    """
    ?

    Parameters
    ----------
    max : int
        hauteur max
        TODO: renommer en h_max pour clarte (car != longueur max)
            Et car max est un mot réservé !
        TODO: idem pour bool

    TODO : Corriger : `bool` est un mot reserve,
           designant la classe des booleens.
    """
    n1, n2 = len(l1), len(l2)
    k1, k2 = 0, 0
    bool = False
    l1bis = l1[:]
    while (k1 < n1) and (not bool):
        l2bis = l2[:]
        k2 = 0
        while (k2 < n2) and (not bool):
            l2bis = [ l2[-k2-1] ] + l2bis[:]
            raise(NotImplementedError(
                "Using forcing1 with non-jugglable patterns."
                ))
            print("----------------------------")
            print("l1bis="+str(l1bis))
            print("l2bis="+str(l2bis))
            bool, rustine = forcing1(l1bis, l2bis, max)
            print("rustine="+str(rustine))
            k2 += 1
        l1bis.append(l1[k1])
        k1 += 1

    return(bool, rustine)
# Que fait cette fonction ?
# In []: forcing2([5,3,1], [3], 10)
# Out[]: (True, [])

# In []: el.forcing2([5,3,1], [4,5,0], 10)
# Ne s'arrete pas


def BAAAAAAH (l1, l2, max, perio):
    etats = [False for k in range (0, max+1)]
    n1, n2 = len(l1), len(l2)
    for k in range (0, n1):
        etats = lancer_sur(etats, l1[k])

    taille_max = 3
    rustine = []
    bool = False
    while (len(rustine) <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0

        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            if k == len(rustine):
                apresperio = etats_test[:]
            bool, etats_test = lancer_verifie(etats_test, liste[k])

            k += 1

        if not bool:
            rustine = suivant_rustine(rustine, max)

    return(bool,
           rustine,
           apresrus,
           apresrus==apresperio
           )