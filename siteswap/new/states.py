"""
Algorithms related to states and state graphs.

States will be list[int] so that several balls can be in the same state,
to allow multiplexes.
After throwing a 1 from state `example_state`, the result will be that
example_state[0] == 1.

So far:
- multiplexes are not allowed;
- everything is asynchronous, but in synchronous fashion, they will be
  list[[int, int]] ; why not numpy.array((*,2)).

###################
### Done / TODO ###
###################
 - TODO :
 Alternative way to find shortest transition:
    go over the juggling graph to find
    one shortest transition (shortest way in a graph, Dijkstra, yeeaaah).

    Indeed, from 1101 (state of 51) to 111 (state of 3) transition_with_states
    gives 440, not 2 or 41 that are far more well-known and simpler.

Heuristic for shorter transitions with states:
    From pattern1 to pattern2 : if there's a smaller gap in state2 than in
    state1, we can throw a ball in it (ex in 1101 to 111, there's a gap of size
    one in state1 (ie there is one 0) so we can throw a 2)



author: Mathis Demay
"""
# built-in
import inspect # to get current function's name
from typing import List

# from pip
import numpy as np

# internal
from siteswap.new.permutation_test import NotJugglableError, is_jugglable

class NotThrowableError(Exception):
    pass

def can_throw_from_state(state: List[int], throw: int):
    """
    Determine if a throw can be performed from a state.
    So far, only vanilla site swap is available (asynchronous and without
    multiplex).

    Parameters
    ----------
    state: list[int]
        A juggling state.
    throw: int
        The throw we would like to perform from `state`.

    Returns
    -------
    bool
        A boolean indicating whether throwing `throw` is possible from `state`.
    """
    if state[0] == 0:
        # No ball is landing
        ret = (throw==0)
    else:     # A ball is landing
        new_state = state[1:] + [0]
        # new_state is the next state just before `throw`:
        # each ball goes down by one level
        if throw >= len(state):
            new_state += (throw - len(new_state)) * [0]
        ret = (new_state[throw-1] == 0)
    return ret


def throw_from_state(state: List[int], throw: int):
    """
    Determine the juggling state after having thrown `throw` from `state`.

    It is assumed that `throw` can be performed from `state`, otherwise
    a NotThrowableError is raised.

    Parameters
    ----------
    state: list[int]
        A juggling state.
    throw: int
        The throw we would like to perform from `state`.

    Return
    ------
    list[int]
        The new state.
    """
    if not can_throw_from_state(state, throw) :
        raise NotThrowableError(
            "Cannot throw "+str(throw)+" from state "+str(state)
            )
    if not throw:
        new_state = state[1:]
    else:
        new_state = state[1:] + [0]
        if throw > len(new_state):
            new_state += (throw-len(new_state))*[0]
        new_state[throw-1] = 1
    return new_state


def get_state_from_pattern(pattern: List[int]):
    """
    Get the state that `pattern` loops.

    It is assumed that `pattern` is jugglable. Otherwise, a NotJugglableError
    is raised.

    Parameters
    ----------
    pattern: list[int]
        The juggling pattern, ie a list of ints which are the throws.
    """
    if not is_jugglable(pattern):
        raise NotJugglableError(str(pattern)+"is not jugglable.")
    state = max(pattern)*[0]
    nb_balls_to_place = int(np.mean(pattern))
    while sum(state) < nb_balls_to_place:
        for throw in pattern:
            state = state[1:]+[0] # time increases by 1
            if throw:
                state[throw-1] = 1

    # Trim possible extra 0s on top
    while not state[-1]:
        del state[-1]

    ############################
    ### Try of numpy version.###
    ############################
    # Problem: for 51, throw only 2 balls, not 3.
    # One solution would be to duplicate the patten enough times : how many ?
    # throws = np.array(pattern)
    # delay = np.arange(len(pattern)-1,-1,-1)
    # ball_final_positions = throws-delay
    # state = np.zeros(np.max(ball_final_positions))
    # state[ball_final_positions]=1
    return state

def transition_with_states(pattern1: List[int], pattern2: List[int]):
    """
    Find transition between `pattern1` and `pattern2` using their states.

    Both patterns are assumed to be jugglable.

    Detail: find the longest part from the top of `pattern1`
    which can be kept in the bottom of `pattern2`.
    Then the length of the transition is the number of levels in the state
    of `pattern1` which can't be kept (ie the length of `pattern1` minus the
    length of the part kept).
    The throws are the heights of the balls in the state of `pattern2`
    which do not come from the part kept from `pattern1`, plus the delay
    (backwards : 0, 1...)

    Notes :
        Mind the zeroes !
        What if the states have different numbers of levels ?

    TODO: Return all site swaps of found transition.
        Some jugglers may prefer one or another.

        Alternative 1 : find the transition of pattern1 to pattern2 or the other
        way round, and then look in the list of jugglable patterns for a pattern
        beginning with this transition. The transition of the other way is the
        end of the pattern.


    Parameters
    ----------
    pattern1 : list[int]
        First jugglable pattern.
    pattern2 : list[int]
        Second jugglable pattern.

    Returns
    -------
    list[int]
        A transition from `pattern1` to `pattern2`
    """
    # Check if both patterns are jugglable and raise error otherwise.
    args = [pattern1, pattern2]
    for arg in args:
        if not is_jugglable(arg):
            current_function_name = str(inspect.stack()[0][3])
            raise NotJugglableError(
                current_function_name + " argument must be jugglable, but " \
                    + str(arg) + " is not.")

    # Check if they have the same number of balls
    if sum(pattern1)/len(pattern1) != sum(pattern2)/len(pattern2):
        raise NotJugglableError(
            "Cannot juggle patterns with different numbers of balls."
        )
    # Get their state
    state1, state2 = get_state_from_pattern(pattern1
                                            ), get_state_from_pattern(pattern2)
    # Determine length of transition
    ## see how many throws can be kept
    can_be_kept = [False] * min(len(state1)+1, len(state2)+1)
    # can_be_kept[i] indicates if the top i levels of energy of state1 can be
    # kept in the bottom of state2
    for k in range(1, min(len(state1)+1,
                          len(state2)+1
                          )
                  ):
        # k = 0 is excluded because it would test all state1 and [] in state2
        # we should test them all and not do a while because
        # eg. [1101] to itself : k=2 yields False and k=4 yields True
        can_be_kept[k] = (state1[-k:] == state2[:k])
    # keep the maximal possible number of throws
    keepable_lengths = [i for i in range(len(can_be_kept)) if can_be_kept[i]]
    if not keepable_lengths:
        length_kept = 0
    else:
        length_kept = max(keepable_lengths)

    # Determine transition
    if length_kept == len(state1):
        # states are identical
        transition = []
    else:
        # We now look at the balls of the bottom part of state1 that are
        # not kept. The heights of their throws are the heights of the balls in
        # state2 which do not come from the part kept from state1,
        # plus the delay (backwards : 0, 1, ..., transition_length-1)
        transition_length = len(state1)-length_kept
        transition = []
        delay = transition_length
        j = length_kept # to go over state2 and find where balls...
        # ...should be after transition
        for i in range(transition_length):
            delay -= 1 # time passes
            if not state1[i]:
                transition.append(0)
            else:
                # find lowest ball still to throw
                while j < len(state2) and not state2[j]:
                    j += 1
                transition.append(j + 1 + delay)
                # the +1 above is due to Python counting from 0 and jugglers
                # thinking the first level of energy will get to the hand
                # in 1 beat so they count from 1
                j += 1
    # numpy version ?
    # delay = np.arange(transition_length-1,-1,-1)
    return transition

def transition_with_throws(pattern1, pattern2):
    """
    Find transition between `pattern1` and `pattern2` using their landing times.

    Detail:
    Consider the juggling diagrams and try to find a transition with 1 throw,
    then 2, ...
    1. Copy `pattern1` until enough different balls are thrown
        (eg 51 has to be copied to 5151)
    2. Idem for `pattern2`
    3. k = 0
    while transition not found :
        Consider the concatenation
            `pattern1`[transition with k throws]`pattern2`
        See when balls from `pattern1` and `pattern2` land
        Find transition so we can throw all balls from `pattern2`
            For that: consider the list of landing times of `pattern2`
            Remove times for which balls come from either pattern (they can come
                from `pattern2` e.g. when you search in 333 ? 5151, the first 1
                lands directly in the following 5)
            If the list of landing times with missing balls is longer than k,
                we could update k right on, couldn't we ?
            if transition not found :
                increase k by 1
    return transition
    """
    return None