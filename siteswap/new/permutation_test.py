"""
Algorithms related to the permutation test and to adjacent throw
exchanges. Contains generating methods, but not only.

Maybe this should be renamed to 'permutation_test' and another file should be
added, for example 'adjacent'.

METHOD: currently, functions are taken in "Fichier global.py" and translated
here.

Ideas to maybe optimize gen_q():
    Idea 1/numpy version : same but with ndarrays
    try :
    a = [[0, 0, 0],
         [0, 0, 0],
         [0, 0, 0]]
    Next : add 1 to every column : 001 010 100
    add 2 to every column : 002 020 200 AND to result of last line : 201 030 300

    Idea 2 : make a tree
    at first element you can choose from 0 to sum
    Then from 0 to (sum-choice 1)...
author: Mathis Demay.
"""
# imports

# built-ins
from typing import List, Union
from numpy.typing import ArrayLike
from copy import deepcopy
from itertools import permutations

# installed
import numpy as np

class NotJugglableError(Exception):
    pass


def incr_list(l: Union[List[int], ArrayLike],
              n_max: int,
              step: int = 1):
    """
    Increment a list, seen as a number in base `n_max`+1, big-endian style
    (basically, as we use to see numbers).

    Work NOT in place.

    Parameters
    ----------
    l : list[int] or array[int]
        A list of numbers.
    n_max : int
        The maximal value of an element of `l`.
    step : int, default 1
        The step by which to increase. The goal is to try and increment directly
        by steps of n (the number of balls).

    Returns
    -------
    Union[List[int], ArrayLike]
        The incremented list.

    TODO : Read this doc and fix the function

    """
    new_l = deepcopy(l) # incremented list
    new_l[-1] += step
    k = 1 # to go over list
    while new_l[-k] > n_max:
        if k == len(l) :
            raise IndexError(
                "[incr_list] Cannot increment list: maximal number reached."
            )
        else:
            q, r = divmod(new_l[-k], n_max+1)
            new_l[-k] = r
            new_l[-k-1] += q
            k += 1
    return new_l


def is_jugglable_n(l: Union[List[int], ArrayLike], n: int):
    """
    Determine if `l` is jugglable with `n` balls.

    Use the necessary condition of the mean (the mean of the throws must be an
    integer, equal to the number of balls) and the permutation test (at each
    time, there should be )
    The necessary condition was originally used to save the cost of the
    permutation test for many sequences (only 1/n sequence passes it) - wait:

    TODO: directly increment with a step of n, and the necessary condition will
    always be satisfied.

    For the moment, only works for asynchronous, non-multiplex juggling.

    Parameters
    ----------
    l : list[int]
        A would-be pattern.
    n : int
        The number of balls expected to juggle `l`.

    Returns
    -------
    bool
        True if and only if `l` is jugglable with `n` balls.
    """
    # Necessary condition: the mean of l must be equal to n
    mean = sum(l)/len(l)
    if mean != n:
        return False

    # The permutation test: necessary and sufficient
    # The idea is to test whether there is a ball landing at each time.
    # For that we compute the arrival time of each throw, which equals the value
    # of the throw plus its departure time.
    p = len(l) # the period
    landing = [(e + t)%p for (e, t) in zip(l, range(p))] # the landing times
    # Then we check if the set of arrival times equals the set of throw times
    return set(landing) == set(range(p))

def is_jugglable(l: Union[List[int], ArrayLike]):
    """
    Determine whether `l` is jugglable.

    Parameters
    ----------
    l : list[int]
        A would-be pattern.

    Returns
    -------
    bool
        True if and only if `l` is jugglable.
    """
    n_balls = sum(l)/len(l)
    return int(n_balls) == n_balls and is_jugglable_n(l, n_balls)

def is_not_circular_permutation(l1: Union[List[int], ArrayLike],
                                l2: Union[List[int], ArrayLike]):
    """
    Find whether `l1` is NOT a circular permutation of `l2`.

    Parameters
    ----------
    l1 : list[int]

    l2 : list[int]

    Returns
    -------
    bool
        True if and only if `l1` is NOT a circular permutation of `l2`.

    """
    if len(l1) != len(l2):
        ret = False
    else:
        i = 0
        while (i < len(l1)) and (l1 != (l2[i:]+l2[:i])):
            i += 1
        ret = (i == len(l1))
    return ret


def not_circular_permutation_in_container(l: List[List], elt: list):
    """
    Find whether `elt` is NOT a circular permutaion of an element of `l`.

    `l` is seen as a "container" of elements of the same fàrm as elt.

    Parameters
    ----------
    l : List[List]
        List containing elements of the same form as `elt` (ie, lists of
        elements with the same type as `elt`'s.)
    elt : list
        The element of which to determine whether it is not a circular
        permutation of any element in `l`.

    Returns
    -------
    bool
        True if and only if `elt` is NOT a circular permutation of any element
        of `l`.

    """
    i = 0 # to go over l
    while (i < len(l)) and is_not_circular_permutation(elt, l[i]):
        i += 1
    return i == len(l)


def generate_circular_permutations(n: int):
    """
    Generate all circular permutations of the integer interval [0...n-1].

    Parameters
    ----------
    n : int
        The number so that the considered interval is [0...n-1] (with elements).

    Returns
    -------
    np.array(int) of shape (n,n)
        All circular permutations of [0...n-1].
    """
    line = np.arange(n)
    col = line.reshape(-1, 1)
    return np.mod(line+col, n)


def normalize(l: Union[List[int], ArrayLike]):
    """
    Normalize a sequence of numbers, meaning give its biggest cyclic shift.

    Example : for [5,7,6], return [7,6,5].

    Parameters
    ----------
    l : list[int] or np.array[int] of dimension 1
        The sequence of numbers to normalize.

    Returns
    -------
    list[int]
        The normalized cyclic shift of l.
    """
    l_arr = np.array(l)
    if len(l_arr.shape) > 1:
        raise ValueError("Cannot normalize array of more than 1 dimension.")
    permutations = generate_circular_permutations(len(l))
    # next line: np.max does not work so we stick with lists
    return max(l_arr[permutations].tolist())


def keep_normalized_elements_in_list(l: Union[List[List[int]], ArrayLike]):
    """
    Return the list of normalized elements from l.

    Parameters
    ----------
    l : list[list[int]] or np.array(int) of dimension 2
        A list containing the lists of ints to normalize.

    Returns
    -------
    list[list[int]]
        The normalized lists of `l`.

    """
    ret = []
    for x in l:
        x_norm = normalize(x)
        if x_norm not in ret:
            ret.append(x_norm)
    return ret


def gene_patterns_naive(period: int, balls: int, height: int):
    """
    Generate all possible patterns with contrained period, number of balls
    and maximum height.

    Naive: use modulo / permutation test, and do not normalize.

    TODO: normalize by representing each pattern by its circular shift which
        is biggest in terms of seeing it as a single number
        (eg take 531 instead of 315 or 153).

    Parameters
    ----------
    period : int
        Period of the generated patterns.
    balls : int
        Number of balls.
    height : int
        Maximum height of throws.

    # Optimization:
      TODO: increment directly by steps of n
      Wait: the ending condition will be harder to find; will we truly optimize?
      For sure this is premature optimization. However it is less dumb than to
      make computations we know are useless.
      We can e.g. in the increment function test if h is reached and why not
      raise a custom error when h is attained everywhere.

    Returns
    -------
    patterns_list : list[list[int]]
        A list containing all patterns under selected constraints.

    """
    current = [0] * period
    if balls == 0 :
        return([current])
    # while ... # make tests for sequences
    # to test if l1 is a circular permutation of l2, we cannot use sets as the
    # order matters ! 423 is jugglable while 432 is not...

    end = [height]*period # word at which to stop the process
    patterns_list = [] # "liste" in french version
    while current != end:
        incr_list(current, height, 1)
        if is_jugglable_n(current, balls
            ) and not_circular_permutation_in_container(patterns_list, current):
            patterns_list.append(current[:]) # to store a copy and not the same
            # list again. The variable of a list in Python is a pointer...
    return patterns_list


def gen_q(b_prime: int, period: int):
    """
    Generate vectors of length `period` with nonnegative ints
    whose sum is exactly `b_prime`.

    Method used:
    Jump from b_prime to b_prime*(b_prime+1)**(period)
    by steps of b_prime
    in base b_prime+1.

    Numpy version.

    Parameters
    ----------
    b_prime : int
        The sum of the vectors.
    period : int
        The number of elements in each vector

    Returns
    -------
    q : np.array(int) of shape (,period)
        array whose every line sums to `b_prime`.
    """
    # The result can be seen as integers in base b'+1, as the vectors q
    # can't contain any digit higher than b'. Then, when enumerating them as
    # a tree of choices with 0 or 1 or... b'-1 at each step, we notice that
    # the first elements are away from each other of exactly a sum of b'
    # (still in base b'+1). So the result is included in the multiples of b'+1.
    # Could we optimize and only get them ? The leaps are higher and higher and
    # probably predictable.

    # Implementation : in Python and numpy I couldn't find a way to compute
    # directly in base b'+1, so I first compute in base 10, then use
    # np.base_repr() convert to base b'+1 (note that this yields strings).

    # get all multiples of b'+1 in base 10
    ints_base_10 = np.arange(b_prime,
                             b_prime*np.power(b_prime+1, period-1)+b_prime,
                             b_prime)
    # convert to base b_prime+1
    ## Make np.base_repr() a vector function to change base
    base_repr_vectorized = np.vectorize(lambda x: np.base_repr(x, b_prime+1))
    str_base_b_prime_plus_1 = base_repr_vectorized(ints_base_10)
    # Pad with 0s at the beginning to have the wanted length
    str_padded = np.where(
       np.char.str_len(str_base_b_prime_plus_1)==period,
       str_base_b_prime_plus_1,
       np.char.add(np.char.multiply(
                        '0',
                        period-np.char.str_len(str_base_b_prime_plus_1)
                    ),
                   str_base_b_prime_plus_1
        )
    )
    # Go from elements of shape '003' to [0,0,3]
    int_list_too_many = np.array([list(x) for x in str_padded]).astype(int)
    # Take out elements of wrong sum
    q = int_list_too_many[np.sum(int_list_too_many, axis=1) == b_prime]
    return q


def gene_patterns_permutation_backwards(period: int, balls: int):
    """
    Generate all jugglable patterns under a defined period, number of balls
    and maximum height.
    Use the method given in 'The Mathematics of Juggling' 2.6.1 (p24)
    which is somewhat the permutation test backwards.

    Parameters
    ----------
    period : int
        The period of the wanted sequences.
    balls : int
        The number of balls to juggle the sequences.

    Returns
    -------
    np.array(int) of shape (-1, period)
        The array of jugglable sequences.
    """
    # Generate all permutations of {0; ...; p-1}
    perm = np.arange(period)
    p = np.array(list(permutations(perm))) # p in the book
    difference = p - perm
    # substract the permutation (0,1...,p-1)
    p_prime_arr = np.where(difference >= 0,
                       difference, difference + period) # p_prime in the book
    # TODO : optimization suggested by Polster : only keep elements of p_prime
    # that are not cyclic shifts of one another.
    # But as we are in Python, doing this with a for loop would considerably
    # slow down the function, more than just making useless computations in
    # numpy.
    p_prime_arr = np.array(keep_normalized_elements_in_list(p_prime_arr))

    # b - mean(P')
    b_prime_arr = (balls - np.mean(p_prime_arr, axis=1)).astype(int)

    # Possibilities for the vector Q
    ## Pb : Q's size varies for each p_prime (ie each b_prime)
    ## so we make a for loop : bad performances.
    ## TODO: check if we could optimize by storing the values of Q in a dict
    ## for the values of b_prime that are equal. In the book's example, b'=2
    ## is used 4 times.
    juggling_sequences = []
    for k in range(b_prime_arr.shape[0]):
        b_prime = b_prime_arr[k]
        q = gen_q(b_prime, period)
        # Juggling sequences =  P' + period * Q
        jugglable = p_prime_arr[k] + period*q
        juggling_sequences += list(jugglable)

    # Trim to keep only patterns that are not cyclic shifts
    juggling_sequences = keep_normalized_elements_in_list(juggling_sequences)
    return np.array(juggling_sequences)
