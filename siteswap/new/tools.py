"""
Generic tools.

author: Mathis Demay.
"""

from copy import deepcopy
import string
from typing import List


def list_to_siteswap(l: List[int]) -> str:
    """
    Convert a list of numbers to its usual site swap representation.
    Example : the list [5, 11, 7] will be converted to '5b7'.

    Parameters
    ----------
    l : list[int]
        A list representing a  site swap pattern.

    Returns
    -------
    str
        The common site swap representation of `l`.

    """
    # first, convert all numbers between 10 and 26 included to letters,
    # starting at 10 |-> a
    # my_l = list(l)
    alphabet = list(string.ascii_lowercase)
    conversion_dict = dict(zip(range(10, 36), alphabet))
    # for k in range(len(my_l)):
    #     e = l[k]
    #     if e in conversion_dict:
    #         l[k] = conversion_dict[e]
    my_l = [conversion_dict[l[k]] if l[k] in conversion_dict else l[k]
            for k in range(len(l))
           ]
    return ''.join(str(e) for e in my_l)
