"""
Algorithms related to exchange of throws, aka site swap.

Author: Mathis Demay.
"""

def swap_adjacent_throws(w, i, j):
    res = w[:] # deepcopy
    if j - i == 1:
        # case when j is right after i : no border effect
        if not w[i]:
            # case when exchange is not possible
            return([])
        else:
            pass
    else:
        # swapping the first and the last : border effect
            pass
