def gene_permu(n):
    return (gpaux([k for k in range(0,n)]))
    #Chapeauuu

def gpaux(liste):
    if len(liste) == 1 :
        return([liste])
    else :
        n = len(liste)
        res = []
        #Echange du premier element avec tous
        for k in range(n):
            aux = liste[:]
            aux[k],aux[0] = aux[0],aux[k]
            sousres = gpaux(aux[1:])
            res += [ [aux[0]] + tortue for tortue in sousres ]
        return(res)

def indice(liste,x):
    k = 0
    while liste[k] != x:
        k += 1
    return k

def listeenpermu(liste):
    n = len(liste)
    pabase = [k for k in range(0,n)]
    res = []
    for k in range(n): # On regarde la k-ieme case de pabase pour y mettre le bon element de liste
        pos_init = indice(pabase,liste[k]) # Ce bon element se trouve au depart a l'indice pos_init
        for elsa in range (k, pos_init): # On le remet a la bonne place telle une bulle qui remonte la liste
            pabase[k + pos_init - elsa],pabase[k+ pos_init - elsa -1] = pabase[k+ pos_init - elsa -1],pabase[k+ pos_init - elsa]
            res += [(k+ pos_init - elsa -1,k+ pos_init - elsa)]
    return(res,pabase)
    
    

def echange_lancers_permissif (mot,i,j):
    mot[i],mot[j] = mot[j] + (j-i) , mot[i] - (j-i)
    return(mot)

def genere_echange_lancers_bis(b,p):
    afaire = [listeenpermu(k) for k in gene_permu(p)[:]]
    n = len(afaire)
    res = []
    cascade = [b]*p
    for k in range (0,n):
        ajout = cascade[:]
        for i in range (0,len(afaire[k])):
            echange_lancers_permissif(ajout, afaire[k][i][0], afaire[k][i][1])
        res += [ajout]
    return res
            