def gene_liste(p):
   fin = 10**p
   nombre = 0
   liste = []
   while nombre != fin :
      elt = [int(k) for k in str(nombre)]
      elt = [0]*(p - len(elt)) + elt
      bool,NB = necessaire(elt)
      if bool :
         liste = liste + [elt]
      nombre += 1
   return(liste)

def necessaire(l):
   somme,long = sum(l),len(l)
   return( somme//long*long == somme , somme//long)

print(gene_liste(3))