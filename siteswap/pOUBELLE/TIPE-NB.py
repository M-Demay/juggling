import random as rng

def appartient(x,l):
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n)

def appartient_val(x,l):
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n,k)

def jonglable(l):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NB = somme/long
    if NB != int(NB) :
        return(0,0)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n,int(NB))

def degage(l,h):
    n = len(l)
    k=0
    for i in range(0,n):
        if not jonglable(l[k],h):
            del l[k]
        else:
            k+=1
    return (l)

def permutation(l1,l2):
    # Teste si l1 est une permutation circulaire de l2
    # On sait qu'on l'utilisera pour len(l1) = len(l2) donc c'est bon
    l = len(l1)
    a,b = appartient_val(l1[0],l2)
    if not a:
        return False
    else:
        for k in range(1,l):
            if (l1[k] != l2[(k+b)%l]):
                return False
    return True

def sans_permutations(l):
    k1=0
    while (k1<len(l)):
        liste = l[k1][0]
        k2=k1+1
        for i in range(k1+1,len(l)):
            if permutation(liste,l[k2][0]):
                del l[k2]
            else:
                k2 += 1
        k1+=1
    return None

def gene_liste(p):
    fin = 10**p
    nombre = 0
    liste = []
    while nombre != fin :
        elt = [int(k) for k in str(nombre)]
        elt = [0]*(p - len(elt)) + elt
        bool,NB = jonglable(elt)
        if bool :
            liste = liste + [(elt,NB)]
        nombre += 1
    sans_permutations(liste)
    return(liste)


def gene_list_NB(p,NB):
    fin = 10**p
    nombre = 0
    liste = []
    while nombre != fin :
        elt = [int(k) for k in str(nombre)]
        elt = [0]*(p - len(elt)) + elt
        if jonglable_NB(elt,NB) :
            liste = liste + [elt]
        nombre += 1
    sans_permutations_NB(liste)
    return(liste)

def gene_list_NB_bis(p,NB,Hmax):
    fin = [Hmax]*p
    nombre = [0]*p
    liste = []
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB):
            liste += [nombre[:]]
    sans_permutations_NB(liste)
    return(liste)

def suivant_liste(liste,max):
    k = 1
    while liste[-k] == max: #On sait que dans son utilisation, on ne depassera jamais la longueur de la liste
        liste[-k] = 0
        k += 1
    liste[-k] += 1
    return None

def jonglable_NB(l,NB):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NBbis = somme/long
    if (NBbis != int(NBbis)) or (NBbis != NB) :
        return(False)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n)

def sans_permutations_NB(l):
    k1=0
    while (k1<len(l)):
        liste = l[k1]
        k2=k1+1
        for i in range(k1+1,len(l)):
            if permutation(liste,l[k2]):
                del l[k2]
            else:
                k2 += 1
        k1+=1
    return None







def canno_logique(liste):
    n = len(liste)
    max,indices = liste[0],[0]
    #Les max et leurs indices
    for k in range(1,n):
        if liste[k] > max:
            max,indices = liste[k],[k]
        elif liste[k] == max:
            indices += [k]
    
    #On enleve à chaque fois l'un des 2 premiers
    while len(indices) != 1:
        k=1
        while (k != n) :
            nb1 = liste[(indices[0] + k) % n]
            nb2 = liste[(indices[1] + k) % n]
            
            if (nb1 > nb2) or (k==n):
                del indices[1]
                k = n #ça nous fait sortir de la boucle
            elif (nb1 < nb2):
                del indices[0]
                k = n
            else:
                k += 1
    
    return reordonner(liste,indices[0])

def reordonner(liste,indice):
    bis = []
    n = len(liste)
    for k in range (0,n):
        bis += [ liste[(indice + k ) % n] ]
    return(bis)

def reordonner_Dieu(liste,indice):
    return (liste[indice:] + liste[:indice])

def cannonise(liste,logique):
    
    def canno_rng(element):
        return(reordonner(element,rng.choice(possibles)))
    
    def canno_logique(liste):
        n = len(liste)
        max,indices = liste[0],[0]
        #Les max et leurs indices
        for k in range(1,n):
            if liste[k] > max:
                max,indices = liste[k],[k]
            elif liste[k] == max:
                indices += [k]
        
        #On enleve à chaque fois l'un des 2 premiers
        while len(indices) != 1:
            k=1
            while (k != n) :
                nb1 = liste[(indices[0] + k) % n]
                nb2 = liste[(indices[1] + k) % n]
                
                if (nb1 > nb2) or (k==n):
                    del indices[1]
                    k = n #ça nous fait sortir de la boucle
                elif (nb1 < nb2):
                    del indices[0]
                    k = n
                else:
                    k += 1
        
        return reordonner(liste,indices[0])
    
    if not logique:
        possibles = [k for k in range(0, len(liste[0]))]
        return([canno_rng(k) for k in liste])
        
    else:
        return([canno_logique(k) for k in liste])












