import sys

sys.setrecursionlimit(10000)

def appartient_true(l): # Fonction particulierement inutilisee.
    if l == [] :
        return(False)
    else :
        return ( l[0] or ( appartient_true(l[1:]) ) )

def appartient_vals(x,l):
    # Renvoie tous les indices ou se trouve x dans l
    n=len(l)
    res = []
    for k in range (n):
        if x == l[k] :
            res += [k]
    return(res)


def egal_perm(l1,l2):
    # Teste si l1 est une permutation circulaire de l2
    # On sait qu'on l'utilisera pour len(l1) = len(l2) donc c'est bon
    n = len(l1)
    valeurs = appartient_vals(l1[0],l2)
    nb_val = len(valeurs)
    if nb_val == 0 : 
        return False
    else:
        # On teste si une des permutations circulaires de l2 qui partent des elements de valeurs est egale a l1
        b = True # Si b est vrai alors on continue
        k = 1 # k represente l'indice du lancer teste
        j = 0 # j parcourt les indices de valeurs
        while ( (j < nb_val) and b ) :
            k = 1 # On n'initialise pas k a 0 car on sait que les lancers l1[0] et l2[valeurs[j]] sont egaux
            while k < n and l1[k] == l2[(valeurs[j]+k)%n] :
                k +=1
            # Si k == n : l1 et l2 sont egaux a permutation circulaire pres. On s'arrete donc.
            b = ( k != n )
            
            j+=1
        return(not b)

def appartient_mot_jonglable(m,l) :
    if l == [] :
        return(False)
    else :
        return ( egal_perm(m,l[0]) or appartient_mot_jonglable(m,l[1:]) )

def echange_adj(m,i,j):
    # Echange les lancers adjacents i et j, i < j
    res = m[:]
    if j - i == 1 : # Cas ou les nombres sont effectivement adjacents dans m
        # Cas ou on ne peut plus echanger les lancers
        if ( m[i] == 0 or m[j] + 1 < m[i] ) :
            return([])
        else :
            nouveau_i = m[j] + 1
            nouveau_j = m[i] - 1
            res[i],res[j] = nouveau_i,nouveau_j
            return(res)
    else : #Cas ou on echange le premier et le dernier de m
        return(echange_adj([m[-1]] + m[:(len(m) - 1)],0,1))

def genere_echanges_adj_ite(b,p):
    # Genere tous les mots jonglables avec une periode p et b balles
    afaire = [p * [b]]
    fait = []
    base = [] # A chaque etape, c'est le premier element de afaire, puis c'est sur lui qu'on travaille ; il faut ensuite l'enlever de afaire, puisqu'il est fait...
    nouveau = []
    while afaire != [] :
        base = afaire[0]
        fait = fait + [base]
        afaire = afaire[1:]
        
        # Tous les echanges sauf dernier-premier
        for i in range(p-1):
            nouveau = echange_adj(base,i,i+1)
            if ( ( nouveau != [] ) and ( not appartient_mot_jonglable(nouveau,fait) ) and ( not appartient_mot_jonglable(nouveau,afaire) ) ) :
                afaire = afaire + [nouveau]
        
        # Cas de l'echange dernier-premier
        nouveau = echange_adj(base,0,2) #Le deuxieme nombre est sans importance tant qu'il est different de 1
        if ( ( nouveau != [] ) and ( not appartient_mot_jonglable(nouveau,fait) ) and ( not appartient_mot_jonglable(nouveau,afaire) ) ) :
            afaire = afaire + [nouveau]
        
    return(fait)

#def genere_echanges_adj_rec(b,p):
# Faire une fonction auxiliaire avec en arguments les listes afaire et fait, puis "fonction chapeau"
    
### BAM

#print(genere_echanges_adj_ite(3,6))
#print(egal_perm([7,4,4,1,7,4,2,3],[1,7,4,2,3,7,4,4]))

## Temps de reponse
import time
def temps_de_reponse(b,p):
 
    debut = time.time()
    genere_echanges_adj_ite(b,p)
    fin = time.time()
    print("temps de reponse :",fin-debut)
#temps_de_reponse(3,7)
