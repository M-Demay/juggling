def suivant_rustine(liste,max):
    n = len(liste)
    if n == 0:
        return [0]
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1):
        liste[-k] += 1
        return(liste)
    else:
        return ([0]*(n+1))

def lancer_sur(etats,h):
    etats = etats[1:] + [False]
    if h != 0:
        etats[h-1] = True
    return (etats)

def lancer_verifie(etats,h):
    if h == 0:
        if etats[0]:
            return False,False
        else:
            return True,(etats[1:] + [False])
    if (not etats[0]):
        return False,False
    if etats[h]:
        return False,False
    else:
        etats = etats[1:] + [False]
        etats[h-1] = True
        return (True,etats)

def forcing1(l1,l2,max):
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1):
        etats = lancer_sur(etats,l1[k])
    
    taille_max = 3
    rustine = []
    bool = False
    while (len(rustine) <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0
        
        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])
            k += 1
        if not bool:
            rustine = suivant_rustine(rustine,max)
        else:
            bool = (apresrus == etats)
    
    return(bool,rustine)

def forcing2(l1,l2,max):
    n1,n2 = len(l1),len(l2)
    k1,k2 = 0,0
    bool = False
    l1bis = l1[:]
    while (k1 < n1) and (not bool):
        l2bis = l2[:]
        k2 = 0
        while (k2 < n2) and (not bool):
            l2bis = [ l2[-k2-1] ] + l2bis[:]
            bool,rustine = forcing1(l1bis,l2bis,max)
            k2 += 1
        l1bis = l1bis[:] + l1[k1]
        k1 += 1
    
    return(bool,rustine)









def BAAAAAAH (l1,l2,max,perio):
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1):
        etats = lancer_sur(etats,l1[k])
    
    taille_max = 3
    rustine = []
    bool = False
    while (len(rustine) <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0
        
        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            if k == len(rustine):
                apresperio = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])
                
            k += 1
        
        if not bool:
            rustine = suivant_rustine(rustine,max)
    
    return(bool,rustine,apresrus,apresrus==apresperio)











