## Les etats seront codes sous forme de listes de booleens. True : il y a une balle. Le premier element etant l'etat le plus bas.
## Methode bourrin : Pour generer l'ensemble des etats a une hauteur h et un nombre de balles n donne, on genere d'abord recursivement tous les etats possibles puis, avec un compteur, on ne garde que ceux qui contiennent le bon nombre de balles.

def genere_tout(h) :
    if h == 1 :
        return([[False],[True]])
    else :
        aux = genere_tout(h-1)
        res = []
        laux = len(aux)
        for k in range(laux) :
            nouveau = [False] + aux[k]
            res = res + [nouveau]
            nouveau = [True] + aux[k]
            res = res + [nouveau]
        #for w in aux :
         #   nouveau = [False] + w
          #  res = res + nouveau
           # nouveau = [True] + w
            #res = res + nouveau
        return(res)


# CTRL MAJ TAB : des lignes bleues apparaissent parfois, pas toujours...
def genere_etats(h,n):
    trop = genere_tout(h)
    ltrop = len(trop)
    
    #Gardons seulement ceux qui nous interessent
    for j in range(0,ltrop):
        # Compteur :
        lk = len(trop[j])
        c = 0
        for k in range(lk) :
            if trop[j][k] :
                c += 1
        if c != n :
            del trop[j]
    return(trop)
    
def genere_etatsv2_aux(h,n):
    # Cette fois on va generer les etats un par un, et les associer a leur nombre de True pour ne pas generer les etats qui ont trop de balles.
    if h == 1 :
        return([ [[False],0] , [[True],1] ])
    else :
        aux = genere_etatsv2_aux(h-1,n)
        laux = len(aux) # Necessaire ou dans le for il ne le calcule qu'au debut ?
        res = []
        for j in range(laux) :
            u,v = aux[j]
            
            #Premier element : le False
            nouveau = u + [False]
            res = res + [ [nouveau,v] ]
            
            #Deuxieme : le True
            if v < n :
                nouveau = u + [True]
                v += 1
                res = res + [ [nouveau,v] ]
        return res

def genere_etatsv2(h,n):
    # On enleve les compteurs devenus inutiles
    aux = genere_etatsv2_aux(h,n)
    laux = len(aux)
    res = []
    for j in range(laux) : #Sinon : res = [aux[k][0] for k in aux]
        res = res + [ aux[j][0] ]
    return(res)
    
            

def lancers_possibles(etat) :
    # Renvoie la liste des lancers possibles a partir d'un etat
    if (not etat[0]) :
        return(|0])
    else :
        letat = len(etat)
        res = []
        for j in range(letat-1) :
            etat[j] = etat[j+1]
            if (not etat[j]) :
                res = res + [j]
        return(res)
        
def etat_suivant(etat,lancer) :
    # Renvoie l'etat dans lequel on se trouve apres avoir effectue un lancer en etant dans un etat
    res = []
    letat = len(etat)
    for j in range(letat - 1) :
        res = res + [ etat[j] ]
    if lancer == 0 :
        res = res + [False]
    else :
        etat[lancer] = True
    return(res)


## Et les excites ?

# Par caracterisation de permutation

def appartient(x,l):
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n)


def jonglable(l):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NB = somme/long
    if NB != int(NB) :
        return(0,0)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)] # Pourquoi pas simplement range(0,n) ?
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n,int(NB))

def excite_permutation(l):
    # Pour un mot jonglable l, determine s'il est excite a l'aide du test de permutation
    somme,long = sum(l),len(l)
    NB = somme/long
    aux = [NB] + l
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)] # Pourquoi pas simplement range(0,n) ?
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n)

# Par methode des etats

def maximum(l):
    if len(l) == 1 :
        return(l[0])
    else :
        aux = maximum(l[1:])
        j = l[0]
        if j >= aux :
            return(j)
        else :
            return(aux)

def excite_etats(l):
    # Pour un mot jonglable l, determine s'il est excite a l'aide de la methode des etats ?
    #Calcul du nombre de balles
    somme,long = sum(l),len(l)
    NB = somme/long
    
    #Generation de l'etat fondamental
    maxi = maximum(l)
    fondam = NB * [True] + (maxi - NB) * [False]

    
    # Comment faut-il faire pour savoir si c'est un excite ou non ? La question ne se reduit pas a : "Est-il possible de faire le lancer l[0] a partir de cet etat ?"... Alors : algorithme trop gourmand ?
    
    
    

    
    
    

## Commentaires

# Fonction qui determine si un siteswap est excite ou non, avec soit la methode des etats, soit le test de permutation : CNS de jonglabilite. Mieux : fonction qui donne ce qu'il faut faire pour le rentrer et le sortir. Plutot facile s'il suffit d'un lancer pour le rentrer, moins facile s'il en faut plusieurs...
# Fonction qui genere les differents siteswaps possibles ? Plus efficace que bourrin ? D'autres methodes existent ? En tout cas naviguer entre les etats implique de parcourir plein de fois toute la liste -> complexite exponentielle?... Mais si en pratique utilise que sur des petits nombres ? Reste que si c'est trop gourmand, bourrin est preferable.
#On sent venir les algorithmes "gloutons"...

## Decouverte
#Determiner quoi faire pour rentrer dans un etat excite. Exemple : de 3 a 51, est-il possible de faire 4 ? On essaie de faire 3 4 51 et on voit ensuite si on se trouve dans un etat permettant de lancer un 5. Alors par re
# FAUX ! GROSSIEREMENT FAUX !
# Il faut partir de l'etat fondamental, puis voir dans quel etat on se trouve apres la combinaison de "rentree" puis voir si apres une boucle de la combinaison excitee, on se trouve dans le meme etat qu'au debut de l'excite. Question : est-on sur alors qu'on peut boucler avec l'excite ?


## Zone de test, attention les yeux !

#print(genere_etats(3,2))
print(genere_etatsv2(4,3))
    
