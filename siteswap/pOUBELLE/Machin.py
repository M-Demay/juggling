import random as rng

def bourrin_fail(h,p):
    temporaire = []
    fin= []
    for k in range (0,p):
        temporaire += [0]
        fin += [h]
    liste = [temporaire]
    print(liste)
    print(fin)
    while liste[-1] != fin:
        aux = (liste[-1])[:]
        ok = True
        k=0
        while k<p and ok:
            if aux[k] != h:
                aux[k] += 1
                ok = False
            else:
                k += 1
        liste += [aux]
        print(liste)
        a=input()
    return (liste)

#NE MARCHE PAS .KHGLJFKHLJHFV.BK

def bourrin(h,p):
    # Genere toutes les combinaisons pour une hauteur maximale h et une periode p
    temporaire = []
    fin= []
    for k in range (0,p):
        temporaire += [0]
        fin += [h]
    liste = [temporaire]
    
    for i in range (0,p):
        l = len(liste)
        ajoutliste = []
        for k in range(0,l):
            terme = (liste[k])[:]
            for j in range(1,h+1):
                ajout = terme[:]
                ajout[i] += j
                ajoutliste += [ajout]
        liste += ajoutliste
    
    return (liste)

def appartient(x,l):
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n)

def appartient_val(x,l):
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n,k)

def jonglable(l):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NB = somme/long
    if NB != int(NB) :
        return(0,0)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n,int(NB))

def degage(l,h):
    n = len(l)
    k=0
    for i in range(0,n):
        if not jonglable(l[k],h):
            del l[k]
        else:
            k+=1
    return (l)


        

def liste_l_N_nul(long,N):
    # Momentanement inutilise
    liste = [long*[0]]
    for i in range (0,N):
        l = len(liste)
        nouvelle_liste=[]
        for k in range(0,l):
            liste_temp = liste[k]
            for m in range(0,long):
                temporaire = liste_temp[:]
                temporaire[m] += 1
                nouvelle_liste += [temporaire]
        liste = nouvelle_liste[:]
    return(liste)


def permutation(l1,l2):
    # Teste si l1 est une permutation circulaire de l2
    # On sait qu'on l'utilisera pour len(l1) = len(l2) donc c'est bon
    l = len(l1)
    a,b = appartient_val(l1[0],l2)
    if not a:
        return False
    else:
        for k in range(1,l):
            if (l1[k] != l2[(k+b)%l]):
                return False
    return True

def sans_permutations(l):
    k1=0
    while (k1<len(l)):
        liste = l[k1][0]
        k2=k1+1
        for i in range(k1+1,len(l)):
            if permutation(liste,l[k2][0]):
                del l[k2]
            else:
                k2 += 1
        k1+=1
    return None

def cannonise(l):
    return None

def liste_jonglable1(h,p):
    liste = bourrin(h,p)
    liste = degage(liste,h) 
    sans_permutations(liste)
    rng.shuffle(liste)
    return (liste)

def gene_liste(p):
    fin = 10**p
    nombre = 0
    liste = []
    while nombre != fin :
        elt = [int(k) for k in str(nombre)]
        elt = [0]*(p - len(elt)) + elt
        bool,NB = jonglable(elt)
        if bool :
            liste = liste + [(elt,NB)]
        nombre += 1
    sans_permutations(liste)
    return(liste)

## Soupe aux choux
