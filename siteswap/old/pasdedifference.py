import numpy as np
import random as rng
import sys
import time
import copy
sys.setrecursionlimit(10000)

##Changements de Luca:

#Ajout d'une version itérative de l'algo de génération d'un "graphe" des figures (classes d'équivalences)
#Ajout d'une variante à l'algo de génération des figures par parcours de l'"automate" : il renvoie la liste des figures ET de toutes leurs permutations circulaires (chacune une et une seule fois) pour enlever des tests de comparaison, et gagner en complexité
#Création d'un algorithme de tri... pythonesque... (et foutrement efficace !!!)
#Création d'un nouvel algorithme générant des numéros de jonglage en exploitant les algorithmes ci-dessus (NB : Il est incroyablement performant par rapport aux autres...)

## Nouveaux changements de Mathis
# Remarque preliminaire : je n'ai pas tout compris aux changements annonces...
# L'echange de lancers a ete ameliore : par normalisation des mots jonglables. Normalisation faite de facon astucieuse : plutot que de rechercher brutalement tous les maxima et de tester si la permutation circulaire du mot partant de chacun de ces maxima est la plus grande dans l'ordre lexicographique, on procede dans la fonction "echange_adj_normalise" ne faisant qu'un test booleen et une concatenation pour determiner lequel est le plus grand. Pour de plus amples precisions, se referer au code de la fonction en question, ou aux explications de l'executant (moi).
# L'autre changement : nouveaute. Algorithme de generation par "remontee du test de permutation" decrit dans le fameux poly d'Herve Devie (ah, si seulement il n'y avait qu'un seul de ses polys qui etait fameux...). J'ai grossierement suivi leurs instructions ; il a fallu notamment determiner la valeur de la variable "somme" de la fonction "permuenmots". Aucune explication n'est ici fournie sur cette valeur. Determinee sur un exemple (4413). Explication a venir sur cette valeur (assez evident en etudiant un exemple).
#Concernant l'algorithme lui-meme : lorqu'on teste la jonglabilite, on ajoute le temps puis on fait le modulo. Ici on fait l'operation reciproque : on termine donc l'algorithme en retirant le temps du lancer. Mais auparavant il faut ajouter la periode a chaque nombre/lancer (operation inverse du modulo). Le tout est de savoir jusqu'ou on ajoute cette periode. Cette valeur est justement celle de somme.
# Explications pas forcement claires mais il fallait les consigner dans le feu de l'action pour n'oublier ni le raisonnement ni la facon de programmer tout ca...
# La condition sur l'echange de lancers est aussi mysterieuse qu'un cours de physique.

## A. Algos de base

def test_permutations_NB(l,elt):
    #Renvoie True si elt n'est PAS une permutation d'un element de l
    # Le NB n'est qu'un relicat, une contingence formellement administrative et desuete
    k=0
    while (k<len(l)) and (not permutation_cheat(elt,l[k])):
        k += 1
    return (k == len(l))

def permutation_cheat(l1,l2):
    #Renvoie True si l2 est une permutation circulaire de l1
    k=0
    while (k < len(l1)) and ((l1[k:] + l1[:k]) != l2):
        k+=1
    return (k != len(l1))

def suivant_liste(liste,max):
    #ajoute 1 à un nombre (qui est sous forme d'une liste)
    k = 1
    while liste[-k] == max: #On sait que dans son utilisation, on ne depassera jamais la longueur de la liste
        liste[-k] = 0
        k += 1
    liste[-k] += 1
    return None

def appartient(x,l):
    #Schema classique de test d'appartenance
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n)

def normalise(mot) : # Renvoie la permutation circulaire la plus grande de mot
    return(max([mot[k:]+mot[:k] for k in range(len(mot)) if mot[k] == max(mot)]))
    
def doublaux(liste,ref): # Fonction auxiliaire pour sansdoublons : transfere les elements de liste dans ref s'ils n'y sont pas
    if liste == [] : return(ref)
    elif liste[0] in ref : return(doublaux(liste[1:],ref))
    else : return(doublaux(liste[1:],[liste[0]]+ref))

def sansdoublons(liste):
    # Renvoie une liste sans doublons ; l'ordre n'importe pas
    return(doublaux(liste,[]))

def egales_ordrepres(l1,l2) :
    n1,n2 = len(l1),len(l2)
    if n1 != n2 : return(False)
    else :
        i = 0
        while i < n1 and l1[i] in l2 :
            i += 1
        return(i == n1)
    
def egalbis(l1,l2) :
    m,n = copy.deepcopy(l1),copy.deepcopy(l2)
    m.sort()
    n.sort()
    return(m == n)

## B. Test de jonglabilite et algorithme de generation par enumeration naive

def jonglable_NB(l,NB):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NBbis = somme/long
    if (NBbis != int(NBbis)) or (NBbis != NB) :
        return(False)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    #Pourquoi ne pas directement ecrire s = [(l[k]+k)%n for k in range(0,n)] ? POURQUOI NE PAS EVITER 2 LIGNES????!!!!!!
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n)

def gene_list_naif(p,NB,Hmax):
    #Genere la liste des figures jonglables (sans permutations)
    fin = [Hmax]*p
    nombre = [0]*p
    liste = []
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB) and test_permutations_NB(liste,nombre):
            liste += [nombre[:]]
    return(liste)

def gene_list_naif_normalise(p,NB,Hmax):
    #Genere la liste des figures jonglables (sans permutations)
    fin = [Hmax]*p
    nombre = [0]*p
    liste = []
    aux = []
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB):
            aux = normalise(nombre[:])
            if not appartient(aux,liste) :
                liste.append(aux)
    return(liste)

def gene_pour_graphe(p,NB,Hmax):
    #Genere la liste des figures jonglables avec toutes les permutations
    fin = [Hmax]*p
    nombre = [0]*p
    liste = []
    aux = []
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB):
            if (not appartient(nombre,liste)):
                for k in range(0,len(nombre)):
                    aux = nombre[k:]+nombre[:k]
                    if (not appartient(aux,liste)):
                        liste += [aux]
    return(liste)
# Ne peut-on pas encore eviter des calculs de nombres...? Ca a l'air dur... Et le double "if" n'est-il pas equivalent a un if (... and ...) ?

## C. Echange de lancers

def appartient_true(l):
    # Fonction particulierement inutilisee.
    if l == [] :
        return(False)
    else :
        return ( l[0] or ( appartient_true(l[1:]) ) )

def appartient_vals(x,l):
    # Renvoie tous les indices ou se trouve x dans l
    n=len(l)
    res = []
    for k in range (n):
        if x == l[k] :
            res += [k]
    # Ou : return([k for k in range(len(l)) if l[k] == x])
    return(res)

def egal_perm(l1,l2):
    # Teste si l1 est une permutation circulaire de l2
    # On sait qu'on l'utilisera pour len(l1) = len(l2)
    n = len(l1)
    valeurs = appartient_vals(l1[0],l2)
    nb_val = len(valeurs)
    if nb_val == 0 : 
        return False
    else:
        # On teste si une des permutations circulaires de l2 qui partent des elements de valeurs est egale a l1
        b = True # Si b est vrai alors on continue
        k = 1 # k represente l'indice du lancer teste
        j = 0 # j parcourt les indices de valeurs
        while ( (j < nb_val) and b ) :
            k = 1 # On n'initialise pas k a 0 car on sait que les lancers l1[0] et l2[valeurs[j]] sont egaux
            while k < n and l1[k] == l2[(valeurs[j]+k)%n] :
                k +=1
            # Si k == n : l1 et l2 sont egaux a permutation circulaire pres. On s'arrete donc.
            b = ( k != n )
            
            j+=1
        return(not b)

def appartient_mot_jonglable(m,l) :
    # Teste si le mot m appartient a la liste l, a permutation circulaire pres
    if l == [] :
        return(False)
    else :
        return ( egal_perm(m,l[0]) or appartient_mot_jonglable(m,l[1:]) )

def echange_adj(m,i,j):
    # Echange les lancers adjacents d'indices i et j, i < j, dans le mot jonglable m
    res = m[:]
    if j - i == 1 : # Cas ou les nombres sont effectivement adjacents dans m
        # Cas ou on ne peut plus echanger les lancers
        if ( m[i] == 0 or m[j] + 1 < m[i] ) : 
        # La condition "m[j] + 1 >= m[i]", dite necessaire pour realiser l'echange, n'est toujours pas claire... Pour 531, le 5 et le 3 ne sont pas echangeables, alors que sur un dessin on y arrive et on n'y voit pas d'obstacle...
            return([])
        else :
            nouveau_i = m[j] + 1
            nouveau_j = m[i] - 1
            res[i],res[j] = nouveau_i,nouveau_j
            return(res)
    else : #Cas ou on echange le premier et le dernier de m
        return(echange_adj([m[-1]] + m[:(len(m) - 1)],0,1))

def genere_echanges_adj_ite(b,p): # Complexite rhedibitoire... Comment eviter de refaire les calculs deja menes ??
    # Genere tous les mots jonglables avec une periode p et b balles, par l'algorithme d'echange de lancers adjacents
    afaire = [p * [b]]
    fait = []
    base = [] # A chaque etape du while, c'est le premier element de afaire, puis c'est sur lui qu'on travaille ; il faut ensuite l'enlever de afaire, puisqu'il est fait...
    nouveau = []  # Pour chaque valeur de "base", prend les valeurs de tous les mots obtenus en echangeant des lancers de "base"
    while afaire != [] :
        # Extraction du premier element, qui devient "base"
        base = afaire.pop()
        fait = fait + [base]
        afaire = afaire
        
        # Tous les echanges sauf dernier-premier
        for i in range(p-1):
            nouveau = echange_adj(base,i,i+1)
            if ( ( nouveau != [] ) and ( not appartient_mot_jonglable(nouveau,fait) ) and ( not appartient_mot_jonglable(nouveau,afaire) ) ) :
                afaire = afaire + [nouveau]
        
        # Cas de l'echange dernier-premier
        nouveau = echange_adj(base,0,2) #Le deuxieme nombre est sans importance tant qu'il est different de 1
        if ( ( nouveau != [] ) and ( not appartient_mot_jonglable(nouveau,fait) ) and ( not appartient_mot_jonglable(nouveau,afaire) ) ) :
            afaire = afaire + [nouveau]
        
    return(fait)

#def genere_echanges_adj_rec(b,p):
# Faire une fonction auxiliaire avec en arguments les listes afaire et fait, puis "fonction chapeau"

## D. Tentative d'amelioration de la complexite de l'algorithme par echange de lancers adjacents
## D.1. Normalisation de toutes les figures par l'ordre lexicographique

def echange_adj_normalise(m,i,j):
    # Echange les lancers adjacents d'indices i et j, i < j, dans le mot jonglable m
    # De plus, sous reserve que le mot donne soit normalise (ce dont on s'assurera dans son utilisation, et qu'on ne refait pas ici puisque le but est de reduire le nombre de calculs), en renvoie un egalement normalise
    #Ne modifie pas le mot donne en argument
    res = m[:]
    if j - i == 1 : # Cas ou les nombres sont effectivement adjacents dans m
        # Cas ou on ne peut plus echanger les lancers
        if ( m[i] == 0 or m[j] + 1 < m[i] ) : # Condition incomprise, Cf premiere version de cet algo
            return([])
        else :
            nouveau_i = m[j] + 1
            nouveau_j = m[i] - 1
            res[i],res[j] = nouveau_i,nouveau_j
            
            #Etape de normalisation : on compare les mots partant des lancers i et 0 car ce sont les deux seuls susceptibles de commencer par le lancer de plus grande valeur. 
            
            return(normalise(res))
           
    else :
        #Cas ou on echange le premier et le dernier de m : comme on est a permutation circulaire pres, on remet le dernier au debut, on fait l'operation comme d'habitude, et la fonction se charge ensuite de renvoyer la plus grande permutation circulaire
        return(echange_adj_normalise([m[-1]]+m[:-1],0,1))
# Avantage sur l'algorithme de generation naive : ici il n'y a que deux candidats a etre la plus grande permutation ; alors que dans l'algo naif il faut tester toutes les possibilites, rien ne permet de reduire le champ des possibles comme ici.
# Desormais, plus besoin de tests d'egalite a permutation circulaire pres : on en a choisi une !

def genere_echanges_adj_ite_normalise(b,p): # Complexite rhedibitoire... Comment eviter de refaire les calculs deja menes ??
    # Genere tous les mots jonglables avec une periode p et b balles, par l'algorithme d'echange de lancers adjacents
    afaire = [p * [b]] # Notons que cet element est normalise, condition necessaire a ce nouvel algorithme
    fait = []
    base = [] # A chaque etape du while, c'est le dernier element de afaire, puis c'est sur lui qu'on travaille ; il faut ensuite l'enlever de afaire, puisqu'il est fait...
    nouveau = []  # Pour chaque valeur de "base", prend les valeurs de tous les mots obtenus en echangeant des lancers de "base"
    while afaire != [] :
        # Extraction du dernier element, qui devient "base"
        base = afaire.pop()
        fait.append(base)
        
        # Tous les echanges sauf dernier-premier
        for i in range(p-1):
            nouveau = echange_adj_normalise(base,i,i+1)
            # Il faudra a nouveau refaire tous les echanges sur lui ; il faut donc l'inserer dans afaire.
            if ( ( nouveau != [] ) and ( not appartient(nouveau,fait) ) and ( not appartient(nouveau,afaire) ) ) :
                afaire.append(nouveau)
        
        # Cas de l'echange dernier-premier
        nouveau = echange_adj_normalise(base,0,2) #Le deuxieme nombre est sans importance tant qu'il est different de 1
        if ( ( nouveau != [] ) and ( not appartient(nouveau,fait) ) and ( not appartient(nouveau,afaire) ) ) :
            afaire.append(nouveau)
        
    return(fait)

def genere_echanges_adj_ite_normalise_hmax(b,p,h): # Complexite rhedibitoire... Comment eviter de refaire les calculs deja menes ??
    # Genere tous les mots jonglables avec une periode p et b balles, par l'algorithme d'echange de lancers adjacents
    afaire = [p * [b]] # Notons que cet element est normalise, condition necessaire a ce nouvel algorithme
    fait = []
    base = [] # A chaque etape du while, c'est le dernier element de afaire, puis c'est sur lui qu'on travaille ; il faut ensuite l'enlever de afaire, puisqu'il est fait...
    nouveau = []  # Pour chaque valeur de "base", prend les valeurs de tous les mots obtenus en echangeant des lancers de "base"
    while afaire != [] :
        # Extraction du dernier element, qui devient "base"
        base = afaire.pop()
        fait.append(base)
        
        # Tous les echanges sauf dernier-premier
        for i in range(p-1):
            nouveau = echange_adj_normalise(base,i,i+1)
            # Il faudra a nouveau refaire tous les echanges sur lui ; il faut donc l'inserer dans afaire.
            if ( ( nouveau != [] ) and ( not appartient(nouveau,fait) ) and ( not appartient(nouveau,afaire) ) ) :
                afaire.append(nouveau)
        
        # Cas de l'echange dernier-premier
        nouveau = echange_adj_normalise(base,0,2) #Le deuxieme nombre est sans importance tant qu'il est different de 1
        if ( ( nouveau != [] ) and ( not appartient(nouveau,fait) ) and ( not appartient(nouveau,afaire) ) ) :
            afaire.append(nouveau)
        
    return([k for k in fait if max(k) <= h])
    
## D.2. Utilisation d'une liste triee pour "faits" dans l'algorithme de generation
# On passe d'une insertion de complexite nulle et une recherche quadratique, a une insertion et une recherche dichotomiques.

def insertion_dichotomique(element,liste):
    if liste == [] : return([element])
    else :
        n = len(liste)//2
        if element < liste[n] : return(insertion_dichotomique(element, liste[:n]) + liste[n:])
        else :
            return(liste[:n+1] + insertion_dichotomique(element,liste[n+1:])) # Jamais d'erreur avec les l[...:] ou l[:...] !!

def recherche_dichotomique(element,liste):
    if liste == [] : return(False)
    else :
        n = len(liste)//2
        if element == liste[n] : return(True)
        elif element < liste[n] : return(recherche_dichotomique(element,liste[:n]))
        else : return(recherche_dichotomique(element,liste[n+1:]))

def genere_echanges_adj_ite_normalise_dichotomique(b,p): # Complexite rhedibitoire... Comment eviter de refaire les calculs deja menes ??
    # Genere tous les mots jonglables avec une periode p et b balles, par l'algorithme d'echange de lancers adjacents
    afaire = [p * [b]] # Notons que cet element est normalise, condition necessaire a ce nouvel algorithme
    fait = []
    base = [] # A chaque etape du while, c'est le dernier element de afaire, puis c'est sur lui qu'on travaille ; il faut ensuite l'enlever de afaire, puisqu'il est fait...
    nouveau = []  # Pour chaque valeur de "base", prend les valeurs de tous les mots obtenus en echangeant des lancers de "base"
    while afaire != [] :
        # Extraction du dernier element, qui devient "base"
        base = afaire.pop()
        insertion_dichotomique(base,fait)
        
        # Tous les echanges sauf dernier-premier
        for i in range(p-1):
            nouveau = echange_adj_normalise(base,i,i+1)
            # Il faudra a nouveau refaire tous les echanges sur lui ; il faut donc l'inserer dans afaire.
            if ( ( nouveau != [] ) and ( not recherche_dichotomique(nouveau,fait) ) and ( not recherche_dichotomique(nouveau,afaire) ) ) :
                afaire.append(nouveau)
        
        # Cas de l'echange dernier-premier
        nouveau = echange_adj_normalise(base,0,2) #Le deuxieme nombre est sans importance tant qu'il est different de 1
        if ( ( nouveau != [] ) and ( not appartient(nouveau,fait) ) and ( not appartient(nouveau,afaire) ) ) :
            afaire.append(nouveau)
        
    return(fait)



## E. Generation par remontee du test de permutation

# Premiere etape : generer les permutations
def gene_permu(n): # Genere sous forme de int list list l'ensemble des permutations de [[1,n]]
    return (gpaux([k for k in range(0,n)]))
    #Chapeauuu

def gpaux(liste):
    if len(liste) == 1 :
        return([liste])
    else :
        n = len(liste)
        res = []
        #Echange du premier element avec tous
        for k in range(n):
            aux = liste[:]
            aux[k],aux[0] = aux[0],aux[k]
            sousres = gpaux(aux[1:])
            res += [ [aux[0]] + tortue for tortue in sousres ]
        return(res)
        
# Ensuite : pour chacune de ces permutations, retrouver l'ensemble des mots jonglables associes : role de permuenmots.
def permuenmots(permutation,b,Hmax): # Permutation est une des permutations renvoyees par gene_permu ; b est le nombre de balles
    p = len(permutation) # C'est la periode
    langage = [permutation] # Ensemble (ou langage)des mots jonglables associes a p par l'algorithme du "test de permutation" de jonglabilite
    mot = permutation
    somme = p * b + p*(p-1)//2 # Somme des chiffres des mots obtenus par ajout des temps aux lancers (dans le sens "habituel" de l'algorithme)
    
    # On ajoute la periode partout jusqu'a ce que la somme du mot fasse somme
    for k in range(p):
        langage = motstropgros(langage,somme,k,p,Hmax)
    langage = [mot for mot in langage if sum(mot) == somme] # Sert a ne garder que les mots dont la somme est la bonne.
    for mot in langage :
        for k in range(p) :
            mot[k] = mot[k] - k
    
    # On supprime les mots contenant des lettres negatives
  
    for k in range(len(langage)): 
        i = 0
        while i < p and langage[k][i]>= 0 :
            i += 1
        if i < p :
            del langage[k]
    return(langage)

def motstropgros(obtenus,somme,rang,periode,Hmax) :
    # Cette fonction, a partir de chaque mot qui se trouve dans "obtenus", ajoute "periode" a l'element d'indice "rang" de ce mot, et ce jusqu'a ce que la somme des elements du mot fasse "somme". Elle consigne egalement tous les resultats intermediaires dans "obtenus".
    # Signification des arguments : mots deja obtenus ; somme qu'il faut atteindre ; rang auquel on en est dans le calcul ; periode.
    aux1 = copy.deepcopy(obtenus) # Il ne faut pas toucher a ceux qu'on obtient ici donc on travaille seulement sur les mots qui se trouvent dans obtenus au debut de l'algorithme. # Heu en fait on peut peut-etre, et ca reviendrait peut-etre a ce qu'on fait actuellement. # Mais en meme temps je ne sais pas ce que donnerait le "for mot in aux1" en faisant evoluer la valeur de aux1 dans ce "for". Ca marche tres bien comme ca.
# Et une deepcopy parce que le [:] ne marche que sur un etage, la deepcopy marche sur tous, et la c'est une int list list...
    aux2 = []
    for mot in aux1 :
        aux2 = mot[:]
        while aux2[rang] <= Hmax and sum(aux2) < somme :
            aux2[rang] += periode
            obtenus.append(aux2[:])
    return(obtenus)
        
def encoreunefacondegenerer(b,p):
    res = []
    permutations = sansdoublons([normalise(x) for x in gene_permu(p)])
    for permu in permutations :
        res += permuenmots(permu,b)
    return(sansdoublons([normalise(x) for x in res]))


# Version de Luca
def test_luca(NB,p,Hmax):
    listePermu = gene_permu(p) # J'ai enleve sansdoublons([normalise(x) for x in gene_permu(p)])
    res = []
    somme = p*NB
    for base in listePermu:
        res += test_luca_aux(base,NB,p,Hmax,somme)
    return sansdoublons([normalise(x) for x in res]) #J'ai enleve sansdoublons ici aussi

def test_luca_aux(liste,NB,p,Hmax,somme):
    # Renvoie la liste des mots jonglables associés à la permutation "liste"
    # liste est la permutation, somme est definie dans test_luca
    if len(liste)==1:
        aux = liste[0] + somme - p +1
        if ((aux>=0) and (aux<=Hmax)):
            return ([ [aux] ])
        else:
            return []
    else:
        res = []
        i=0
        n = len(liste)
        while (liste[0] + i*p - (p-n)) <= Hmax:
            if (liste[0] + i*p - (p-n)) >=0:
                res += [ [liste[0] + i*p - (p-n)] + k for k in test_luca_aux(liste[1:],NB,p,Hmax,somme - i * p) ]
            i+=1
        return res
    # Premiere amelioration : on ne fait que des +, on ne retourne pas chercher l[0] et on ne recalcule pas len(liste). On peut encore reduire le nombre de calculs, mais c'est moins lisible, et il faut calculer "liste[0] + i*p - (p-len(liste))" et aussi "somme - i * p"
    #else :
       # res = []
        #elt = l[0]
        #n = len(liste)
        #compte = 0
        #while elt + compte - (p-n) <= Hmax :
       #     if elt + compte -(p-n) >= 0 :
       #         res +=[ [elt + compte - (p-n)] + k for k in test_luca_aux(liste[1:],NB,p,Hmax,somme - compte) ] # En fait on fait le calcul a l'envers non ? On retire plus au debut qu'a la fin ! Evidemment le resultat est le meme.
        #    compte += p
       # return(res)
        

## F. Rustines

#Rustines naives

def suivant_rustine(liste,max):
    #ajoute 1 à la rustine (qui est sous forme d'une liste) et l'agrandit si necessaire
    n = len(liste)
    if n == 0:
        return [0]
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1):
        liste[-k] += 1
        return(liste)
    else:
        return ([0]*(n+1))
  
def lancer_sur(etat,h):
    #Donne l'état d'arrivée après un lancer à partir d'un état antérieur (on sait que l'on a le droit de faire ce lancer)
    etat = etat[1:] + [False]
    if h != 0:
        etat[h-1] = True
    return (etat)

def lancer_verifie(etat,h):
    #Donne l'état d'arrivée après un lancer à partir d'un état antérieur EN INDIQUANT si on a le droit de faire ce lancer ou non
    if h == 0:
        if etat[0]:
            return False,False
        else:
            return True,(etat[1:] + [False])
    if (not etat[0]):
        return False,False
    if etat[h]:
        return False,False
    else:
        etat = etat[1:] + [False]
        etat[h-1] = True
        return (True,etat)



def forcing1(l1,l2,max): #Dans les faits, l'utiliser avec l1*10, l2*10
    #Recherche naive d'une transition entre 2 figures
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1):
        etats = lancer_sur(etats,l1[k])
    
    taille_max = 6
    rustine = []
    lr = 0
    bool = False
    while (lr <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0
        
        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])
            k += 1
        if not bool:
            rustine = suivant_rustine(rustine[:],max)[:]
            lr = len(rustine)
    
    return(bool,rustine)

#rustines entre etats

def genere_etat(mot,NB,Hmax):
    #Donne l'état qui est conservé par une figure
    etats = [False for k in range(0,Hmax+1)] # Oui ou directement (Hmax+1)*[False], pourquoi faire quelque chose de simple et clair, on risquerait de comprendre...
    while sum(etats) != NB:
        for k in range(0,len(mot)):
            etats = lancer_sur(etats,mot[k])
    return(etats)


def indiceEmboitage(etat1,etat2):
    #Donne l'indice de la liste à partir duquel on peut etre passé d'un état à un autre
    etattest = etat1[:] + [False for k in etat1]
    k = 0
    ok = False
    while not ok:
        indices = []
        i=0
        while (i< len(etat1)) and ((etattest[k+i] - etat2[i]) <= 0):
            if (etattest[k+i] - etat2[i]) != 0:
                indices += [k+i]
            i += 1
        ok = (i == len(etat1))
        k += 1
    return(k-1,indices)


def rustine_etats(etat1,etat2):
    #Recherche plus optimale d'une transition entre 2 figures
    i,indices = indiceEmboitage(etat1,etat2)
    rustine = []
    ii=0
    for k in range (0,i):
        if not etat1[k]:
            rustine += [0]
        else:
            rustine += [indices[ii] - k]
            ii += 1
    return rustine

## G. Graphes (figures)

def graph_emonde(graph):
    #On ne conserve qu'un element par classe d'equiv (ie l'etat pour graph_bis ci-après)
    return([k[0] for k in graph])

#Les "bis" de tous les programmes suivants n'ont pas été enlevés par crainte d'en oublier (et que certains programmes en appellent d'autres qui ont changé de nom)

def graphaux_bis(jonglables,graphe,NB,Hmax): #se basant sur les états
    #Fonction recursive pour générer le graphe des figures jonglables
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables.pop()
        n = len(graphe)
        i = 0
        etat = genere_etat(mot,NB,Hmax)
        while i<n :
            candidat = graphe[i][0] #Graphe de la forme : [etat,figure1,figure2,figure3,...],[etat',figure1',figure2',figure3',...],...
            if candidat == etat:
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[etat,mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux_bis(jonglables,graphe,NB,Hmax))

def graph_iter(jonglables,NB,Hmax): #version test
    graphe = []
    while jonglables != []:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables[0]
        del jonglables[0]
        n = len(graphe)
        i = 0
        etat = genere_etat(mot,NB,Hmax)
        while i<n :
            candidat = graphe[i][0] #Graph de la forme : [etat,figure1,figure2,figure3,...],[etat',figure1',figure2',figure3',...],...
            if candidat == etat:
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[etat,mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
    return(graphe)

def echainnement_dans_graph_bis(graph,Hmax): # /!\ UTILISE UN GRAPHE EMONDE
    #Donne la liste des enchainnement correspondant à chaque classe d'équivalence
    echainnement = [[] for k in range(0,len(graph))]
    for k in range (0, len(graph)):
        for i in range (0, len(graph)):
            echainnement[k] += [rustine_etats(graph[k],graph[i])]
    return(echainnement)

def graph_bis(p,NB,Hmax):
    #Fonction chapeau qui renvoie le graphe des figures jonglables, et leurs transitions
    graphe = graphaux_bis(gene_pour_graphe(p,NB,Hmax),[],NB,Hmax)
    return([k[1:] for k in graphe],echainnement_dans_graph_bis(graph_emonde(graphe),Hmax)) #pour des soucis de lisibilité, on dissocie les classes d'equiv (desquelles on a enlevé les etats) des transition
    
#PS: Caml est un language de COMPOSITION... :P

def graph_bis_numero(liste,NB,Hmax):
    #Idem, mais prend en argument la liste des figures, et plus la periode
    graphe = graphaux_bis(liste,[],NB,Hmax)
    return([k[1:] for k in graphe],echainnement_dans_graph_bis(graph_emonde(graphe),Hmax))

def graph_bis_numero_iter(liste,NB,Hmax): #Version test
    #Idem, mais prend en argument la liste des figures, et plus la periode
    graphe = graph_iter(liste,NB,Hmax)
    return([k[1:] for k in graphe],echainnement_dans_graph_bis(graph_emonde(graphe),Hmax))

def graphEnListe(graphe):
    #Transforme le graphe en une liste où chaque element est associé au numéro de sa classe d'équivalence
    liste = []
    for k in range (0,len(graphe)):
        for mot in graphe[k]:
            liste += [ [mot,k] ]
    return liste

## H. Automates (états)


def ens_etats(b,h):
    # Renvoie la liste des etats pour b balles et une hauteur de h
    if b == h:
        return([[True]*b])
    if b == 0 :
        return([[False]*h])
    else:
        JSP1 = ens_etats(b-1,h-1)
        JSP2 = ens_etats(b, h-1)
        return [[True] + k  for k in JSP1] + [[False] + k for k in JSP2]

def lancers_possibles(etat) :
    # Renvoie la liste des lancers possibles a partir d'un etat
    if (not etat[0]) :
        return([0])
    else :
        res = []
        etatbis = etat[1:] + [False]
        for k in range (0,len(etatbis)):
            if not etatbis[k]:
                res += [k+1]
        return(res)

def etat_suivant(etat,lancer) :
    # Renvoie l'etat dans lequel on se trouve apres avoir effectue un lancer en etant dans un etat
    res = etat[1:] + [False]
    if lancer != 0 :
        res[lancer-1] = True
    return(res)


def position(etat,graph):
    # On sait que etat se trouve dans graph ; on renvoie sa position dans graph (indice dans la liste donc a partir de 0)
    if graph[0][0] == etat :
        return(0)
    else :
        return(1 + position(etat, graph[1:]))
    

def graphecomplet(b,h):
    # Renvoie un graphe de la forme [ [ etat, [lancer1, position_de_l'etat_d'arrivee1], [lancer2, ...] , ... ] , [autre_etat,...] ]
    graph = [ [k] for k in ens_etats(b,h) ]
    for k in graph :
        etat = k[0]
        for j in lancers_possibles(etat):
            etatprime = etat_suivant(etat,j)
            pos = position(etatprime,graph)
            k += [[j,pos]]
    return(graph)

def figureParGraphe_aux(graph,perio,Netat,entree):
    # graph est de la forme renvoyee par 
    #Parcourt le graphe en largeur avec un nombre "periode" de tansitions, à partir de l'état "entree", en retenant les lancers effectués
    #Renvoie la succession des lancers ssi on est retourné (après un nombre "periode" de lancers) à l'état de départ ("entree") ie ssi la figure conserve l'état ie ssi elle est jonglable
    if perio == 1:
        return [[k[0]] for k in graph[Netat][1:] if k[1] == entree]
    else:
        res = []
        for k in (graph[Netat][1:]):
            res += [[k[0]] + i for i in figureParGraphe_aux(graph,perio-1,k[1],entree)]
        return res

def fusionPermuPres(l1,l2):
    #Fusion de liste en ne repetant pas les figures identiques à permutations circulaires près (= union ensembliste)
    return (l1 + [k for k in l2 if test_permutations_NB(l1,k)])

def figureParGraphe(graph,perio):
    #Lance "figureParGraphe_aux" à partir de chaque état du graphe, et renvoie l'ensemble des figures jonglables
    res = []
    for k in range(0,len(graph)):
        res = fusionPermuPres(res,figureParGraphe_aux(graph,perio,k,k))
    return res

def figureParGraphe_plage(graph,perioMin,perioMax):
    #Idem, mais plus optimisé pour une "plage" de périodes
    res = []
    for k in range(0,len(graph)):
        res = fusionPermuPres(res,figureParGraphe_plage_aux(graph,perioMin,perioMax,k,k)) #inutile en pratique de verifier les egalités à permu. près pour le graphe : on veut toutes les permus -> cf algo suivant
    return res

def figureParGraphe_plage_test_amelio(graph,perioMin,perioMax):
    #Idem, mais plus optimisé pour une "plage" de périodes
    res = []
    for k in range(0,len(graph)):
        res += figureParGraphe_plage_aux(graph,perioMin,perioMax,k,k)
    return res

def figureParGraphe_plage_aux(graph,perioMin,perioMax,Netat,entree):
    #Idem mais avec une plage de périodes
    if perioMax == 1:
        return [[k[0]] for k in graph[Netat][1:] if k[1] == entree]
    elif perioMin <= 1:
        res = [[k[0]] for k in graph[Netat][1:] if k[1] == entree]
        for k in (graph[Netat][1:]):
            res += [[k[0]] + i for i in figureParGraphe_plage_aux(graph,perioMin-1,perioMax-1,k[1],entree)]
        return res
    else:
        res = []
        for k in (graph[Netat][1:]):
            res += [[k[0]] + i for i in figureParGraphe_plage_aux(graph,perioMin-1,perioMax-1,k[1],entree)]
        return res

# Versions avec normalisation

def fusion_sans_doublon(l1,l2):
    #Fusion de liste en ne repetant pas les figures de l2 qui sont deja presentes dans l1. On suppose que tous les elements de l1 et l2 sont dans l'ordre lexicographique
    return (l1 + [k for k in l2 if not k in l1])

def figureParGraphe_normalise(graph,perio):
    #Lance "figureParGraphe_aux" à partir de chaque état du graphe, et renvoie l'ensemble des figures jonglables
    res = []
    for k in range(0,len(graph)):
        res = fusion_sans_doublon(res,[normalise(k) for k in figureParGraphe_aux(graph,perio,k,k)])
    return res

## I. Difficulté + tri

def difficulte(mot,NB,Hmax,longMax):
    #Attribue une difficulté aux figures
    n = len(mot)
    
    #Longueur de la periode
    difPerio = n/longMax
    
    #Ecart-type ; je m'arrange pour qu'il soit compris entre 0 et 1
    moyenne = NB /Hmax #Ici, j'ai pris "max", mais j'aurais aussi pu prendre le lancer le plus haut présent dans la liste
    var = 0
    for k in mot:
        var += (k/Hmax - moyenne)*(k/Hmax - moyenne) * 4 #En fait, c'est la variance sans multiplier par la proba( = 1/n)
    difEC = np.sqrt(var / n)
    
    #Différence de hauteur de lancers deux à deux adjacents
    somme = 0
    for k in range (0,n):
        somme += abs(mot[k-1] - mot[k])/Hmax
    difAdj = somme/n
    
    res = difPerio*(1/3) + difEC*(1/3) + difAdj*(1/3) #Il faudra surement choisir des coefficients plus réflechis que ça pour les différentes difficultés
    
    return res


def tri_quadra(liste,NB,Hmax,longMax):
    #Tri par insersion en quadratique
    liste = [ k + [ difficulte(k[0],NB,Hmax,longMax) ] for k in liste ]
    res = [ [0,0,2] ] #2 est une difficulté absurde pour eviter les depassements de liste, on elevera cet elt à la fin
    for k in liste:
        i=0
        while k[2] > res[i][2]:
            i+=1
        res = res[:i] + [k] + res[i:]
    return res[:-1]

#def tri_lin(liste,
#    return(res[1:-1])
            
def tri_python(liste,NB,Hmax,longMax): #Moi ?! Faire du pyhton pythonesque ?! JAMAIS !NB,Hmax,longMax):
    liste = [ k + [ difficulte(k[0],NB,Hmax,longMax) ] for k in liste ]
    liste.sort(key=lambda elt: elt[2])
    return(liste)

def partitionDifficulte(liste,nombre):
    # utilise une liste de tri_lin/quadra ; nombre = nb de groupe de difficulté (le dernier sera le plus gros si ça ne tombe pas juste)
    #Sépare la liste en un nombre "nombre" de listes en fonction de leur difficulté (de la plus faible à la plus élevée)
    n = len(liste)
    NpG = n // nombre
    res = []
    indice =0
    
    for k in range(0,nombre -1): #On gere le dernier à part
        resaux = []
        for i in range(0,NpG):
            resaux += [liste[indice][:-1]]
            indice += 1
        res += [resaux]
        
    resaux =[]
    while indice < n:
        resaux += [liste[indice][:-1]]
        indice += 1
        
    return (res + [resaux])

#On a donc des groupes de difficultés dont les elts sont des listes de 2 elts : le premier qui est le mot jonglable, et le second qui est son etat
#Pour les echainner, on utilise donc la liste des transitions donnée par graph_bis


## J. Numéros

def numeroBogo(PerioMin,PerioMax,NB,Hmax,lancers):
    # "Stupide" (ie sans logique),bien pour les numéros courts (< 100 lancers)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    
    cascade = [NB]*PerioMax #On commence et on finit par elle
    
    listeFigures = graphEnListe(graphe)
    #gpFigure = partitionDifficulte(tri_lin(listeFigure,NB,Hmax,PerioMax),5) #5 à parametrer en fct de "lancers"
    #En fait pas besoin de difficulté ici
    numero = cascade[:]
    etat = 0
    
    while len(numero) < lancers:
        choix = rng.choice(listeFigures)
        numero += enchainnements[etat][(choix[1])]
        etat = choix[1]
        iteration = rng.choice(range(2,5)) #à éditer
        for k in range(0,iteration):
            numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    return numero

def fonctionDifficulte(x,a,b): #a entre 1 et 10 (entre 1 et 5 pour les fct), b entre 1 et 2 #On veut a ou b augmente => difficulté augmente #a est la difficulté "principale", et b la "secondaire"
    #Fonction qui donne une bijection de [0,1] pour donner des figures en moyenne plus ou moins difficiles
    if a < 6:
        return (1 - x**((a/5)/(3-b))) #Bidouillage sur les facteurs de a et b pour respecter les condictions ci-dessus
    else:
        return (x**(((11-a)/5)/b)) #idem

def fonctionDifficulteBis(x,a):
    #Idem en enlevant le paramètre b
    if a < 6:
        return (1 - x**(a/5)) 
    else:
        return (x**((11-a)/5))


def numeroLogiqueLong(PerioMin,PerioMax,NB,Hmax,lancers):
    #Bien pour les numéros longs (>500-1000)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    
    cascade = [NB]*PerioMax #On commence et on fini par elle
    
    ngp = 5 #correspond au nombre de groupes de difficultés, il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    gpFigure = partitionDifficulte(tri_lin(listeFigures,NB,Hmax,PerioMax),ngp)
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5
    
    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait, er on considère que l'on ne fait que figures de PerioMax avec des rustine de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1) #Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)
    
    if NbFigure < 50:
        bMax = 5
    elif NbFigure < 200:
        bMax = 10
    elif NbFigure < 500:
        bMax = 15
    else:
        bMax = 20
    
    aMax = NbFigure // bMax
    
    for a in range (0,aMax+1):
        for b in range (0,bMax+1):
            choixDifficulte = fonctionDifficulte(rng.random(),1 + ( (a/aMax)*4 ), 1 + ( b/bMax ) )
            k=1
            enCours = True
            while enCours:
                if k / ngp >= choixDifficulte:
                    enCours = False
                    k -= 1 #Pour correspondre à l'indice de la liste
                else:
                    k+=1
            choix = rng.choice(gpFigure[k])
            numero += enchainnements[etat][(choix[1])]
            etat = choix[1]
            iteration = rng.choice(range(iterationMin,iterationMax+1))
            for k in range(0,iteration):
                    numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    return numero

def numeroLogiqueLong_ListeFigureAutomate(PerioMin,PerioMax,NB,Hmax,lancers): # a=numeroLogiqueLong_ListeFigureAutomate(2,8,6,9,1000)
    #Bien pour les numéros longs (>500-1000)
    t1= time.time()
    listeFigures = figureParGraphe_plage_test_amelio(graphecomplet(NB,Hmax),PerioMin,PerioMax)
    print("figures générées ,", time.time() -t1 )
    t2= time.time()
    graphe,enchainnements = graph_bis_numero_iter(listeFigures,NB,Hmax)
    print("graphe généré,", time.time() -t2)
    cascade = [NB]*PerioMax #On commence et on finit par elle
    
    ngp = 5 #correspond au nombre de groupes de difficulté, il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    t3= time.time()
    gpFigure = partitionDifficulte(tri_python(listeFigures,NB,Hmax,PerioMax),ngp)
    print("difficultés générées,", time.time() -t3)
    
    t4= time.time()
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5
    
    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait, er on considère que l'on ne fait que figures de PerioMax avec des rustine de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1) #Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)
    
    if NbFigure < 50:
        bMax = 5
    elif NbFigure < 200:
        bMax = 10
    elif NbFigure < 500:
        bMax = 15
    else:
        bMax = 20
    
    aMax = NbFigure // bMax
    
    for a in range (0,aMax+1):
        for b in range (0,bMax+1):
            choixDifficulte = fonctionDifficulte(rng.random(),1 + ( (a/aMax)*4 ), 1 + ( b/bMax ) )
            k=1
            enCours = True
            while enCours:
                if k / ngp >= choixDifficulte:
                    enCours = False
                    k -= 1 #Pour correspondre à l'indice de la liste
                else:
                    k+=1
            choix = rng.choice(gpFigure[k])
            numero += enchainnements[etat][(choix[1])]
            etat = choix[1]
            iteration = rng.choice(range(iterationMin,iterationMax+1))
            for k in range(0,iteration):
                    numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    print("numéro généré,", time.time() -t4)
    
    return numero

def numeroLogiqueCourt(PerioMin,PerioMax,NB,Hmax,lancers):
    #Bien pour les numéro de tailles intermédiaires (On a enlevé b)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    cascade = [NB]*PerioMax #On commence et on fini par elle
    
    ngp = 5 #correspond au nombre de groupes de difficultés, il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    gpFigure = partitionDifficulte(tri_lin(listeFigures,NB,Hmax,PerioMax),ngp)
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5
    
    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait, er on considère que l'on ne fait que figures de PerioMax avec des rustine de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1) #Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)
    
    aMax = NbFigure
    
    for a in range (0,aMax+1):
        choixDifficulte = fonctionDifficulteBis(rng.random(),1 + ( (a/aMax)*4 ))
        k=1
        enCours = True
        while enCours:
            if k / ngp >= choixDifficulte:
                enCours = False
                k -= 1 #Pour correspondre à l'indice de la liste
            else:
                k+=1
        choix = rng.choice(gpFigure[k])
        numero += enchainnements[etat][(choix[1])]
        etat = choix[1]
        iteration = rng.choice(range(iterationMin,iterationMax+1))
        for k in range(0,iteration):
                numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    return numero





def TempsEnLancers(NbSecondes):
    return(NbSecondes*3)


## K. Debugage

def listeEnNombre(liste):
    res =""
    for k in liste:
        res+= str(k)
    return int(res)

def nombreEnListe(nombre):
    return([int(k) for k in list(str(nombre))])

def indicePlantage(numero):
    etat = [True,True,True,True,True,True] + [ False for k in range(0,10)]
    i=0
    for k in numero:
        bool,etat = lancer_verifie(etat,k)
        if not bool:
            return i
        i+=1
    return True

def test_graph1(PerioMin,PerioMax,NB,Hmax):
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    return (enchainnements)


def cherche_erreur(PerioMin,PerioMax,NB,Hmax):
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    for k in range (0,len(graphe)):
        etat = genere_etat(graphe[k][0],3,9) + [False,False,False]
        for i in range(0,len(graphe)):
            transition = enchainnements[k][i]
            etattest= etat[:]
            for j in transition:
                bool,etattest = lancer_verifie(etattest,j)
                if not bool:
                    return (k,i)
            if etattest != (genere_etat(graphe[i][0],3,9) + [False,False,False]):
                return (k,i)
    return True



def test():
    truc1 = figureParGraphe_plage(graphecomplet(3,6),2,5)
    truc2 = gene_list_naif(2,3,6)+gene_list_naif(3,3,6)+gene_list_naif(4,3,6)+gene_list_naif(5,3,6)
    return (truc1 == fusionPermuPres(truc1,truc2) and truc2 == fusionPermuPres(truc2,truc1))


def tempsDeReponse(PerioMin,PerioMax,NB,Hmax,lancers):
    t= time.time()
    a=numeroLogiqueLong_ListeFigureAutomate(PerioMin,PerioMax,NB,Hmax,lancers)
    print(time.time() - t)
    t= time.time()
    a=numeroLogiqueLong(PerioMin,PerioMax,NB,Hmax,lancers)
    print(time.time() - t)

def doublons(liste):
    a= liste
    for k in range(0,len(liste)):
        for i in range(k+1, len(liste)):
            if permutation_cheat(liste[k],liste[i]):
                return True,k,i
    return False

def egal(PerioMin,PerioMax,NB,Hmax):
    liste1 = figureParGraphe_plage_test_amelio(graphecomplet(NB,Hmax),PerioMin,PerioMax)
    liste2=[]
    for k in range(PerioMin,PerioMax+1):
        liste2 += gene_pour_graphe(k,NB,Hmax)
    
    liste1.sort()
    liste2.sort()
    return(liste1==liste2)

def test_tri(PerioMin,PerioMax,NB,Hmax,lancers):
    t1= time.time()
    listeFigures = figureParGraphe_plage_test_amelio(graphecomplet(NB,Hmax),PerioMin,PerioMax)
    print("figures générées ,", time.time() -t1 )
    t2= time.time()
    graphe,enchainnements = graph_bis_numero_iter(listeFigures,NB,Hmax)
    print("graphe généré,", time.time() -t2)
    cascade = [NB]*PerioMax #On commence et on fini par elle
    
    ngp = 5 #correspond au nombre de groupes de difficultés, il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    t3= time.time()
    gpFigure1 = tri_lin(listeFigures,NB,Hmax,PerioMax)
    gpFigure2 = tri_python(listeFigures,NB,Hmax,PerioMax)
    print("difficultés générées,", time.time() -t3)
    return (gpFigure1,gpFigure2)

## L. Temps de reponse d'une fonction

import time
def temps_de_reponse(f,b,p):
    debut = time.time()
    f(b,p)
    fin = time.time()
    print("temps de reponse :",fin-debut)
    #C'est le temps de reponse (ou presque) en secondes.

def temps_de_reponse_liste(b,p,h):
    Hmax = h
    NB = b
    debut = time.time()
    gene_list_naif_normalise(p,NB,Hmax)
    algo1 = time.time() - debut
    genere_echanges_adj_ite_normalise_hmax(b,p,h)
    algo2 = time.time() - debut - algo1
    test_luca(NB,p,Hmax)
    algo3 = time.time() - debut - algo2
    #figureParGraphe_normalise(graphecomplet(b,h),p)
    algo4 = time.time() - debut - algo3
    fin = time.time()
    print("temps de reponse du naif                : ",algo1)
    print("temps de reponse par echange de lancers : ",algo2)
    print("temps de reponse par remontee           : ",algo3)
    print("temps de reponse par graphe             : ",algo4)

#Les fonction de génération sans permutation normalisés : [gene_list_naif_normalise(p,NB,Hmax), genere_echanges_adj_ite_normalise_hmax(b,p,h) , test_luca(NB,p,Hmax) , figureParGraphe_normalise(graph,perio) ]
    
## M. Aide au diaporama

import matplotlib.pyplot as plt
import random as rng
def f(x):
    return(x**(0.8)) # 0.8 ou 2/7.5
    
def troce():
    x=[0]
    y=[f(0)]
    for k in range(1,101):
        x=x+[x[-1] + 0.01]
        y=y+[f(x[-1])]
    plt.clf()
    plt.axis('equal')
    i = rng.random()
    #plt.xlim([0.0,1.0])
    #plt.ylim([0.0,1.0])
    plt.plot([i,i,-0.2],[0,f(i),f(i)])
    plt.plot(x,y)
    plt.show()
    return([i,f(i)])

def g(x): return(1/15 + 0.9 * x + np.sin(45*x)/15)

def triste():
    x=[0]
    y=[g(0)]
    for k in range(1,101):
        x=x+[x[-1] + 0.01]
        y=y+[g(x[-1])]
    plt.clf()
    plt.axis('equal')
    #plt.axis([0.5,0.51,0,1])
   
    plt.plot(x,y)
    plt.xlim([0.5,0.51])
    plt.ylim([0.0,1.0])
    plt.show()
    return()
    
    