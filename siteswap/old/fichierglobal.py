import numpy as np
import random as rng
import sys
sys.setrecursionlimit(10000)

##Algos de base

def gene_list_naif(p,NB,Hmax):
    """ En cours dans new.permutation_test.gene_patterns_naive """
    #Genere la liste des figures jonglables (sans permutations)
    fin = [Hmax]*p # dernier nombre a examiner (stop quand on le rencontre)
    nombre = [0]*p # nombre courant examine
    liste = [] # liste des valides
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB) and test_permutations_NB(liste,nombre):
            liste += [nombre[:]]
    return(liste)

def gene_pour_graphe(p,NB,Hmax):
    #Genere la liste des figures jonglables avec toutes les permutations
    fin = [Hmax]*p
    nombre = [0]*p
    liste = []
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB):
            if (not appartient(nombre,liste)):
                for k in range(0,len(nombre)):
                    if (not appartient(nombre[k:] + nombre[:k],liste)):
                        liste += [nombre[k:] + nombre[:k]]
    return(liste)

def test_permutations_NB(l,elt):
    """ Fait dans not_circular_permutation_in_container. """
    #Renvoie VRAI si elt n'est PAS une permutation d'un element de l
    # Le NB n'est qu'un relicat, une contingence formellement administrative
    # et desuete
    k=0
    while (k<len(l)) and (not permutation_cheat(elt,l[k])):
        k += 1
    return (k == len(l))

def permutation_cheat(l1,l2):
    """ Fait dans not_circular_permutation. """
    #Renvoie True si l2 est une permutation circulaire de l1
    k=0
    while (k < len(l1)) and ((l1[k:] + l1[:k]) != l2):
        k+=1
    return (k != len(l1))

def suivant_liste(liste,max):
    """ Fait, sous generate.incr_list. """
    #ajoute 1 à un nombre (qui est sous forme d'une liste)
    # En place !
    k = 1
    while liste[-k] == max:
        # On sait que dans son utilisation, on ne depassera jamais
        # la longueur de la liste
        liste[-k] = 0
        k += 1
    liste[-k] += 1
    return None


def appartient(x,l):
    """ Inutile dans la nouvelle version ("in" dans Python suffit et est
    probablement plus efficace). """
    #Schema classique de test d'appartenance
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n)

def jonglable_NB(l,NB):
    """ Fait dans jugglable_n. """
    # Condition necessaire
    somme,long = sum(l),len(l)
    NBbis = somme/long
    if (NBbis != int(NBbis)) or (NBbis != NB) :
        return(False)

    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5
        #Pour Python, True=1 et False=0 ...
        #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul
        # est vrai.
        k+=1
    return (k==n)

## Echange de lancers

def appartient_true(l):
    """ Inutile également dans la nouvelle version. """
    # Fonction particulierement inutilisee.
    if l == [] :
        return(False)
    else :
        return ( l[0] or ( appartient_true(l[1:]) ) )

def appartient_vals(x,l):
    # Renvoie tous les indices ou se trouve x dans l
    n = len(l)
    res = []
    for k in range (n):
        if x == l[k] :
            res += [k]
    return(res)


def egal_perm(l1,l2):
    """ Fait la même chose que permutation_cheat non ? """
    # Teste si l1 est une permutation circulaire de l2
    # On sait qu'on l'utilisera pour len(l1) = len(l2) donc c'est bon
    n = len(l1)
    valeurs = appartient_vals(l1[0],l2)
    nb_val = len(valeurs)
    if nb_val == 0 :
        return False
    else:
        # On teste si une des permutations circulaires de l2 qui partent
        # des elements de valeurs est egale a l1
        b = True # Si b est vrai alors on continue
        k = 1 # k represente l'indice du lancer teste
        j = 0 # j parcourt les indices de valeurs
        while ( (j < nb_val) and b ) :
            k = 1 # On n'initialise pas k a 0 car on sait que les lancers
            # l1[0] et l2[valeurs[j]] sont egaux
            while k < n and l1[k] == l2[(valeurs[j]+k)%n] :
                k +=1
            # Si k == n : l1 et l2 sont egaux a permutation circulaire pres.
            # On s'arrete donc.
            b = ( k != n )

            j+=1
        return(not b)

def appartient_mot_jonglable(m,l) :
    """ Fait dans not_circular_permutation_in_container ."""
    # Teste si le mot m appartient a la liste l
    if l == [] :
        return(False)
    else :
        return ( egal_perm(m,l[0]) or appartient_mot_jonglable(m,l[1:]) )

def echange_adj(m,i,j):
    # Echange les lancers adjacents i et j, i < j
    res = m[:]
    if j - i == 1 : # Cas ou les nombres sont effectivement adjacents dans m
        # Cas ou on ne peut plus echanger les lancers
        if ( m[i] == 0 or m[j] + 1 < m[i] ) :
            return([])
        else :
            nouveau_i = m[j] + 1
            nouveau_j = m[i] - 1
            res[i],res[j] = nouveau_i,nouveau_j
            return(res)
    else : #Cas ou on echange le premier et le dernier de m
        return(echange_adj([m[-1]] + m[:(len(m) - 1)],0,1))

def genere_echanges_adj_ite(b,p):
    # Genere tous les mots jonglables avec une periode p et b balles
    afaire = [p * [b]]
    fait = []
    base = [] # A chaque etape, c'est le premier element de afaire,
    # puis c'est sur lui qu'on travaille ; il faut ensuite l'enlever de afaire,
    # puisqu'il est fait...
    nouveau = []
    while afaire != [] :
        base = afaire[0]
        fait = fait + [base]
        afaire = afaire[1:]

        # Tous les echanges sauf dernier-premier
        for i in range(p-1):
            nouveau = echange_adj(base,i,i+1)
            if ( ( nouveau != [] ) and (
             not appartient_mot_jonglable(nouveau,fait)
             ) and ( not appartient_mot_jonglable(nouveau,afaire) ) ) :
                afaire = afaire + [nouveau]

        # Cas de l'echange dernier-premier
        nouveau = echange_adj(base,0,2)
        # Le deuxieme nombre est sans importance tant qu'il est different de 1
        if ( ( nouveau != [] ) and (
         not appartient_mot_jonglable(nouveau,fait)
         ) and (
         not appartient_mot_jonglable(nouveau,afaire) ) ) :
            afaire = afaire + [nouveau]

    return(fait)

#def genere_echanges_adj_rec(b,p):
# Faire une fonction auxiliaire avec en arguments les listes afaire et fait,
# puis "fonction chapeau"

##Rustines

#Rustines naives

def suivant_rustine(liste,max):
    # ajoute 1 à la rustine (qui est sous forme d'une liste) et l'aggrandi
    # si necessaire
    n = len(liste)
    if n == 0:
        return [0]
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1):
        liste[-k] += 1
        return(liste)
    else:
        return ([0]*(n+1))

def lancer_sur(etats,h):
    # Donne l'état d'arrivée après un lancer à partir d'un état antérieur
    # (on sait que l'on a le droit de faire ce lancer)
    etats = etats[1:] + [False]
    if h != 0:
        etats[h-1] = True
    return (etats)

def lancer_verifie(etats,h):
    # Donne l'état d'arrivé après un lancer à partir d'un état antérieur
    # EN INDIQUANT si on a le droit de faire ce lancer ou non
    if h == 0:
        if etats[0]:
            return False,False
        else:
            return True,(etats[1:] + [False])
    if (not etats[0]):
        return False,False
    if etats[h]:
        return False,False
    else:
        etats = etats[1:] + [False]
        etats[h-1] = True
        return (True,etats)



def forcing1(l1,l2,max): #Dans les faits, l'utiliser avec l1*10, l2*10
    #Recherche naive d'une transition entre 2 figures
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1):
        etats = lancer_sur(etats,l1[k])

    taille_max = 6
    rustine = []
    lr = 0
    bool = False
    while (lr <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0

        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])
            k += 1
        if not bool:
            rustine = suivant_rustine(rustine[:],max)[:]
            lr = len(rustine)

    return(bool,rustine)

#rustines entre etats

def genere_etat(mot,NB,Hmax):
    #Donne l'état qui est conservé par une figure
    etats = [False for k in range(0,Hmax+1)]
    while sum(etats) != NB:
        for k in range(0,len(mot)):
            etats = lancer_sur(etats,mot[k])
    return(etats)


def indiceEmboitage(etat1,etat2):
    # Donne l'indice de la liste à partir duquel on peut etre passé
    # d'un état à un autre
    etattest = etat1[:] + [False for k in etat1]
    k = 0
    ok = False
    while not ok:
        indices = []
        i=0
        while (i< len(etat1)) and ((etattest[k+i] - etat2[i]) <= 0):
            if (etattest[k+i] - etat2[i]) != 0:
                indices += [k+i]
            i += 1
        ok = (i == len(etat1))
        k += 1
    return(k-1,indices)


def rustine_etats(etat1,etat2):
    #Recherche plus optimale d'une transition entre 2 figures
    i,indices = indiceEmboitage(etat1,etat2)
    rustine = []
    ii=0
    for k in range (0,i):
        if not etat1[k]:
            rustine += [0]
        else:
            rustine += [indices[ii] - k]
            ii += 1
    return rustine

##Graphes (figures)

def graph_emonde(graph):
    #On ne conserve qu'un element par classe d'equiv
    # (ie l'etat pour graph_bis ci-après)
    return([k[0] for k in graph])

#Les "bis" de tous les programmes suivant n'ont pas été enlevé par crainte
# d'en oublier (et que certains programmes en appellent d'autre
# qui ont changé de nom)

def graphaux_bis(jonglables,graphe,NB,Hmax): #se basant sur les états
    #Fonction recursive pour générer le graphe des figures jonglables
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant
        # d'utiliser cette fonction!!!
        mot = jonglables.pop()
        n = len(graphe)
        i = 0
        etat = genere_etat(mot,NB,Hmax)
        while i<n :
            candidat = graphe[i][0]
            # Graph de la forme : [etat,figure1,figure2,figure3,...],
            # [etat',figure1',figure2',figure3',...],...
            if candidat == etat:
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[etat,mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux_bis(jonglables,graphe,NB,Hmax))


def echainnement_dans_graph_bis(graph, Hmax): # /!\ UTILISE UN GRAPHE EMONDE
    # Donne la liste des enchainnement correspondant à chaque classe
    # d'équivalence
    echainnement = [[] for k in range(0,len(graph))]
    for k in range (0, len(graph)):
        for i in range (0, len(graph)):
            echainnement[k] += [rustine_etats(graph[k],graph[i])]
    return(echainnement)

def graph_bis(p,NB,Hmax):
    # Fonction chapeau qui renvoie le graphe des figures jonglables,
    # et leurs transitions
    graphe = graphaux_bis(gene_pour_graphe(p,NB,Hmax),[],NB,Hmax)
    return([k[1:] for k in graphe],
        echainnement_dans_graph_bis(graph_emonde(graphe),Hmax)
    ) #pour des soucis de lisibilité, on dissocie les classes d'equiv
    # (desquelles on a enlevé les etats) des transition

#PS: Caml est un language de COMPOSITION... :P

def graph_bis_numero(liste,NB,Hmax):
    #Idem, mais prend en argument la liste des figures, et plus la periode
    graphe = graphaux_bis(liste,[],NB,Hmax)
    return([k[1:] for k in graphe],
        echainnement_dans_graph_bis(graph_emonde(graphe),Hmax))


def graphEnListe(graphe):
    #Transforme le graphe en une liste où chaque element est associé au
    # numéro de sa classe d'équivalence
    liste = []
    for k in range (0,len(graphe)):
        for mot in graphe[k]:
            liste += [ [mot,k] ]
    return liste

##Automates (états)


def ens_etats(b,h):
    # Renvoie la liste des etats pour b balles et une hauteur de h
    if b == h:
        return([[True]*b])
    if b == 0 :
        return([[False]*h])
    else:
        JSP1 = ens_etats(b-1,h-1)
        JSP2 = ens_etats(b, h-1)
        return [[True] + k  for k in JSP1] + [[False] + k for k in JSP2]

def lancers_possibles(etat) :
    # Renvoie la liste des lancers possibles a partir d'un etat
    if (not etat[0]) :
        return([0])
    else :
        res = []
        etatbis = etat[1:] + [False]
        for k in range (0,len(etatbis)):
            if not etatbis[k]:
                res += [k+1]
        return(res)

def etat_suivant(etat,lancer) :
    # Renvoie l'etat dans lequel on se trouve apres avoir effectue un lancer
    # en etant dans un etat
    res = etat[1:] + [False]
    if lancer != 0 :
        res[lancer-1] = True
    return(res)


def position(etat,graph):
    # On sait que etat se trouve dans graph ; on renvoie sa position
    # dans graph (indice dans la liste donc a partir de 0)
    if graph[0][0] == etat :
        return(0)
    else :
        return(1 + position(etat, graph[1:]))


def graphecomplet(b, h):
    # Renvoie un graphe de la forme [
    # [ etat, [lancer1, position_de_l'etat_d'arrivee1],
    #         [lancer2, ...] , ...
    # ] ,
    #   [autre_etat,...]
    # ]
    graph = [ [k] for k in ens_etats(b,h) ]
    for k in graph :
        etat = k[0]
        for j in lancers_possibles(etat):
            etatprime = etat_suivant(etat,j)
            pos = position(etatprime,graph)
            k += [[j,pos]]
    return(graph)

def figureParGraphe_aux(graph,perio,Netat,entree):
    # Parcours le graph en largeur avec un nombre "periode" de tansitions,
    # à partir de l'état "entree", en retenant les lancers effectués
    # Renvoie la succession des lancers ssi on est retourné
    # (après un nombre "periode" de lancers) à l'état de départ ("entree")
    # ie ssi la figure conserve l'état ie ssi elle est jonglable
    if perio == 1:
        return [[k[0]] for k in graph[Netat][1:] if k[1] == entree]
    else:
        res = []
        for k in (graph[Netat][1:]):
            res += [
                [k[0]] + i for i in figureParGraphe_aux(graph,
                                                        perio-1,
                                                        k[1],
                                                        entree)
            ]
        return res

def fusionPermuPres(l1,l2):
    # Fusion de liste en ne repetant pas les figures identiques
    # à permutations circulaires près (= union ensembliste)
    return (l1 + [k for k in l2 if test_permutations_NB(l1,k)])

def figureParGraphe(graph,perio):
    # Lance "figureParGraphe_aux" à partir de chaque état du graphe,
    # et renvoie l'ensemble des figures jonglables
    res = []
    for k in range(0,len(graph)):
        res = fusionPermuPres(res,figureParGraphe_aux(graph,perio,k,k))
    return res

def figureParGraphe_plage(graph,perioMin,perioMax):
    #Idem, mais plus optimisé pour une "plage" de périodes
    res = []
    for k in range(0,len(graph)):
        res = fusionPermuPres(res,
           figureParGraphe_plage_aux(graph,perioMin,perioMax,k,k)
        )
    return res

def figureParGraphe_plage_aux(graph,perioMin,perioMax,Netat,entree):
    #Idem mais avec une plage de périodes
    if perioMax == 1:
        return [[k[0]] for k in graph[Netat][1:] if k[1] == entree]
    elif perioMin <= 1:
        res = [[k[0]] for k in graph[Netat][1:] if k[1] == entree]
        for k in (graph[Netat][1:]):
            res += [
              [k[0]] + i for i in figureParGraphe_plage_aux(
                graph,perioMin-1,perioMax-1,k[1],entree
              )
            ]
        return res
    else:
        res = []
        for k in (graph[Netat][1:]):
            res += [
                [k[0]] + i for i in figureParGraphe_plage_aux(
                    graph,perioMin-1,perioMax-1,k[1],entree)
                ]
        return res

##Difficulté + tri

def difficulte(mot,NB,Hmax,longMax):
    #Attribue une difficulté aux figures
    n = len(mot)

    #Longueur de la periode
    difPerio = n/longMax

    #Ecart-type ; je m'arrange pour qu'il soit compris entre 0 et 1
    moyenne = NB /Hmax #Ici, j'ai pris "max", mais j'aurais aussi pu prendre
    # le lancer le plus haut présent dans la liste
    var = 0
    for k in mot:
        var += (k/Hmax - moyenne)*(k/Hmax - moyenne) * 4
        #En fait, c'est la variance sans multiplier par la proba( = 1/n)
    difEC = np.sqrt(var / n)

    #Différence de hauteur de lancers deux à deux adjacents
    somme = 0
    for k in range (0,n):
        somme += abs(mot[k-1] - mot[k])/Hmax
    difAdj = somme/n

    res = difPerio*(1/3) + difEC*(1/3) + difAdj*(1/3)
    # Il faudra surement choisir des coefficients plus réflechis que ça
    # pour les différentes difficultés

    return res


def tri_quadra(liste,NB,Hmax,longMax):
    #Tri par insersion en quadratique
    liste = [ k + [ difficulte(k[0],NB,Hmax,longMax) ] for k in liste ]
    res = [ [0,0,2] ] #2 est une difficulté absurde pour eviter les
    # depassements de liste, on elevera cet elt à la fin
    for k in liste:
        i=0
        while k[2] > res[i][2]:
            i+=1
        res = res[:i] + [k] + res[i:]
    return res[:-1]

def tri_lin(liste,NB,Hmax,longMax):
    #dichoto #Et oui, un tri en nlog(n) est linéaire,
    # tout le monde sait ça <(^.^)>
    #Tri par insersion en O(n*log(n))
    liste = [ k + [ difficulte(k[0],NB,Hmax,longMax) ] for k in liste ]
    res = [ [0,0,-1],[0,0,2] ] #Idem que précédemment
    for k in liste:
        n = len(res)
        u = n-1
        l = 0
        i = (u+l) // 2
        while ( k[2] > res[i+1][2] ) or (k[2] <= res[i][2]):
                l = i
            else:
                u = i
            i = (u+l)//2
        res = res[:i+1] + [k] + res[i+1:]
    return(res[1:-1])


def partitionDifficulte(liste,nombre):
    # utilise une liste de tri_lin/quadra ;
    # nombre = nb de groupe de difficulté (le dernier sera le plus gros
    # si ça ne tombe pas juste)
    # Sépare la liste en un nombre "nombre" de listes en fonction de leur
    # difficulté (de la plus faible à la plus élevée)
    n = len(liste)
    NpG = n // nombre
    res = []
    indice =0

    for k in range(0,nombre -1): #On gere le dernier à part
        resaux = []
        for i in range(0,NpG):
            resaux += [liste[indice][:-1]]
            indice += 1
        res += [resaux]

    resaux =[]
    while indice < n:
        resaux += [liste[indice][:-1]]
        indice += 1

    return (res + [resaux])

#On a donc des groupes de difficultés dont les elts sont des listes de 2 elts :
# le premier qui est le mot jonglable, et le second qui est son etat
#Pour les echainner, on utilise donc la liste des transitions
# donnée par graph_bis


##Numéros

def numeroBogo(PerioMin,PerioMax,NB,Hmax,lancers):
    # "Stupide" (ie sans logique),bien pour les numéros courts (< 100 lancers)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)

    cascade = [NB]*PerioMax #On commence et on fini par elle

    listeFigures = graphEnListe(graphe)
    #gpFigure = partitionDifficulte(tri_lin(listeFigure,NB,Hmax,PerioMax),5)
    #5 à parametrer en fct de "lancers"
    #En fait pas besoin de difficulté ici
    numero = cascade[:]
    etat = 0

    while len(numero) < lancers:
        choix = rng.choice(listeFigures)
        numero += enchainnements[etat][(choix[1])]
        etat = choix[1]
        iteration = rng.choice(range(2,5)) #à éditer
        for k in range(0,iteration):
            numero += choix[0]

    numero += enchainnements[etat][0] + cascade[:]

    return numero

def fonctionDifficulte(x,a,b): #a entre 1 et 10 (entre 1 et 5 pour les fct),
# b entre 1 et 2
#On veut a ou b augmente => difficulté augmente
#a est la difficulté "principale", et b la "secondaire"
#Fonction qui donne une bijection de [0,1] pour donner des figures
# en moyenne plus ou moins difficiles
    if a < 6:
        return (1 - x**((a/5)/(3-b))) #Bidouillage sur les facteurs de a et b
        # pour respecter les condictions ci-dessus
    else:
        return (x**(((11-a)/5)/b)) #idem

def fonctionDifficulteBis(x,a):
    #Idem en enlevant le paramètre b
    if a < 6:
        return (1 - x**(a/5))
    else:
        return (x**((11-a)/5))


def numeroLogiqueLong(PerioMin,PerioMax,NB,Hmax,lancers):
    #Bien pour les numéro longs (>500-1000)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)

    cascade = [NB]*PerioMax #On commence et on fini par elle

    ngp = 5 #correspond au nombre de groupes de difficultés,
    # il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    gpFigure = partitionDifficulte(tri_lin(listeFigures,NB,Hmax,PerioMax),ngp)
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5

    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait,
    # er on considère que l'on ne fait que figures de PerioMax avec des rustine
    # de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1)
    # Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)

    if NbFigure < 50:
        bMax = 5
    elif NbFigure < 200:
        bMax = 10
    elif NbFigure < 500:
        bMax = 15
    else:
        bMax = 20

    aMax = NbFigure // bMax

    for a in range (0,aMax+1):
        for b in range (0,bMax+1):
            choixDifficulte = fonctionDifficulte(
                rng.random(),1 + ((a/aMax)*4), 1 + ( b/bMax )
            )
            k=1
            enCours = True
            while enCours:
                if k / ngp >= choixDifficulte:
                    enCours = False
                    k -= 1 #Pour correspondre à l'indice de la liste
                else:
                    k+=1
            choix = rng.choice(gpFigure[k])
            numero += enchainnements[etat][(choix[1])]
            etat = choix[1]
            iteration = rng.choice(range(iterationMin,iterationMax+1))
            for k in range(0,iteration):
                    numero += choix[0]

    numero += enchainnements[etat][0] + cascade[:]

    return numero

def numeroLogiqueCourt(PerioMin,PerioMax,NB,Hmax,lancers):
    #Bien pour les numéro de tailles intermédiaires (On a enlevé b)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    cascade = [NB]*PerioMax #On commence et on fini par elle

    ngp = 5 #correspond au nombre de groupes de difficultés,
    # il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    gpFigure = partitionDifficulte(tri_lin(listeFigures,NB,Hmax,PerioMax),ngp)
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5

    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait,
    # er on considère que l'on ne fait que figures de PerioMax avec des rustines
    # de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1)
    # Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)

    aMax = NbFigure

    for a in range (0,aMax+1):
        choixDifficulte = fonctionDifficulteBis(rng.random(),1 + ( (a/aMax)*4 ))
        k=1
        enCours = True
        while enCours:
            if k / ngp >= choixDifficulte:
                enCours = False
                k -= 1 #Pour correspondre à l'indice de la liste
            else:
                k+=1
        choix = rng.choice(gpFigure[k])
        numero += enchainnements[etat][(choix[1])]
        etat = choix[1]
        iteration = rng.choice(range(iterationMin,iterationMax+1))
        for k in range(0,iteration):
                numero += choix[0]

    numero += enchainnements[etat][0] + cascade[:]

    return numero





def TempsEnLancers(NbSecondes):
    return(NbSecondes*3)


##Debugage

def listeEnNombre(liste):
    res =""
    for k in liste:
        res+= str(k)
    return int(res)

def nombreEnListe(nombre):
    return([int(k) for k in list(str(nombre))])

def indicePlantage(numero):
    etat = [True,True,True] + [ False for k in range(0,10)]
    i=0
    for k in numero:
        bool,etat = lancer_verifie(etat,k)
        if not bool:
            return i
        i+=1
    return True

def test_graph1(PerioMin,PerioMax,NB,Hmax):
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    return (enchainnements)


def cherche_erreur(PerioMin,PerioMax,NB,Hmax):
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    for k in range (0,len(graphe)):
        etat = genere_etat(graphe[k][0],3,9) + [False,False,False]
        for i in range(0,len(graphe)):
            transition = enchainnements[k][i]
            etattest= etat[:]
            for j in transition:
                bool,etattest = lancer_verifie(etattest,j)
                if not bool:
                    return (k,i)
            if etattest != (
                genere_etat(graphe[i][0],3,9) + [False,False,False]):
                return (k,i)
    return True



def test():
    truc1 = figureParGraphe_plage(graphecomplet(3,6),2,5)
    truc2 = gene_list_naif(2,3,6)+gene_list_naif(3,3,6
        )+gene_list_naif(4,3,6)+gene_list_naif(5,3,6)
    return (truc1 == fusionPermuPres(truc1,truc2
        ) and truc2 == fusionPermuPres(truc2,truc1))
