import numpy as np

def suivant_rustine(liste,max):
    n = len(liste)
    if n == 0:
        return [0]
    # On recherche le premier element inferieur au max
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1):
        # Dans ce cas c'est la condition liste[-k]==max qui n'est pas remplie :
        # on augmente donc cette valeur de 1
        liste[-k] += 1
        return(liste)
    else:
        # Dans ce cas tout le monde est egal au max ; on teste donc les
        # rustines de longueur superieure.
        return ([0]*(n+1))

def lancer_sur(etats,h):
    # Sous reserve que le lancer de hauteur h soit realisable dans l'etat etats,
    # cette fonction actualise l'etat.
    etats = etats[1:] + [False]
    if h != 0:
        etats[h-1] = True
    return (etats)

def lancer_verifie(etats,h):
    # Renvoie un booleen pour dire si on peut faire le lancer h dans l'etat
    # etats, et l'etat suivant le cas echeant.
    if h == 0:
        if etats[0]:
            return False,False
        else:
            return True,(etats[1:] + [False])
    #A partir d'ici le lancer n'est pas un 0
    if (not etats[0]): #Cas ou on n'a pas de balle a lancer
        return False,False
    if etats[h]: # Cas ou l'"endroit d'arrivee" est deja occupe
        return False,False
    else: # Si on est arrive ici c'est que tout va bien, on peut faire le lancer.
        etats = etats[1:] + [False]
        etats[h-1] = True
        return (True,etats)

def graphaux(jonglables,graphe,NB):
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument !
        # Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables.pop()
        n = len(graphe)
        i = 0
        while i<n :
            candidat = graphe[i][0]
            if jonglable_NB(mot + candidat,NB):
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux(jonglables,graphe,NB))


def jonglable_NB(l,NB):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NBbis = somme/long
    if (NBbis != int(NBbis)) or (NBbis != NB) :
        return(False)

    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5
        #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n)

def ajoutelesrustines(liste):
    res = [[patate] for patate in liste]
    for patate in liste:
        for i in range (0,len(liste)):
            a,b = forcing1(patate[0]*10, liste[i][0])[1] #*10 a deplacer
            #a finir







def forcing1(l1,l2,max):
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1): # C'est peut-etre ce passage qui est faux.
        etats = lancer_sur(etats,l1[k])

    taille_max = 3
    rustine = []
    lr = 0 # C'est la longueur de la rustine
    bool = False
    while (lr <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0

        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])
            k += 1
        if not bool:
            rustine = suivant_rustine(rustine[:],max)[:]
            lr = len(rustine)

    return(bool,rustine)

def forcing2(l1,l2,max):
    n1,n2 = len(l1),len(l2)
    k1,k2 = 0,0
    bool = False
    l1bis = l1[:]
    while (k1 < n1) and (not bool):
        l2bis = l2[:]
        k2 = 0
        while (k2 < n2) and (not bool):
            l2bis = [ l2[-k2-1] ] + l2bis[:]
            bool,rustine = forcing1(l1bis,l2bis,max)
            k2 += 1
        l1bis = l1bis[:] + l1[k1]
        k1 += 1

    return(bool,rustine)




##Retour aux elucubrations de Luca
def difficile_naif(liste,max):
    n = len(liste)

    #Le nombre de zéros
    compt = 0.0
    for k in liste:
        if k ==0:
            compt += 1
    res = (1 -(compt / n)) /4 #Il faudra voir si certains paramètres sont plus
    # importants que d'autres (actuellement, ils sont tous à 25%)

    #Longueur de la période (période max = 10)
    res += ((float(n)) / 10.0) /4

    #Hauteur des lancers
    compt = 0.0
    for k in liste:
        compt += float(k) / max
    res += (compt / n) / 4

    #Ecart-type ; je m'arrange pour qu'il soit compris entre 0 et 1
    moyenne = (float(sum(liste)) / n) /max
    # Ici, j'ai pris "max", mais j'aurais aussi pu prendre le lancer le plus haut
    # présent dans la liste
    var = 0
    for k in liste:
        var += (float(k)/max - moyenne)*(float(k)/max - moyenne) * 4
        #En fait, c'est la variance sans multiplier par la proba( = 1/n)
    EC = np.sqrt(var / n)
    res += EC /4

    return(res,EC)




def BAAAAAAH (l1,l2,max,perio):
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1):
        etats = lancer_sur(etats,l1[k])

    taille_max = 3
    rustine = []
    bool = False
    while (len(rustine) <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0

        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            if k == len(rustine):
                apresperio = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])

            k += 1

        if not bool:
            rustine = suivant_rustine(rustine,max)

    return(bool,rustine,apresrus,apresrus==apresperio)


##TEST
#Ligne suivante : il faut executer le fichier Echange de lancers car c'est la que la fonction genere_echanges_adj_ite est definie
#jonglables = genere_echanges_adj_ite(3,3)
#print(graphaux3(jonglables,3,10))
