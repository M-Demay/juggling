import numpy as np
import random as rng
import sys
sys.setrecursionlimit(10000)

#generation de la liste des moths jonglables
#Changelog : ajout de "gene_pour_graphe" qui ajoute toutes les permutations

def gene_pour_graphe(p,NB,Hmax):
    fin = [Hmax]*p
    nombre = [0]*p
    liste = []
    if NB == 0:
        return([nombre])
    while nombre != fin :
        suivant_liste(nombre,Hmax)
        if jonglable_NB(nombre,NB):
            for k in range(0,len(nombre)):
                if (not appartient(nombre[k:] + nombre[:k],liste)):
                    liste += [nombre[k:] + nombre[:k]]
    return(liste)

def test_permutations_NB(l,elt):
    k=0
    while (k<len(l)) and (not permutation_cheat(elt,l[k])):
        k += 1
    return (k == len(l))

def suivant_liste(liste,max):
    k = 1
    while liste[-k] == max: #On sait que dans son utilisation, on ne depassera jamais la longueur de la liste
        liste[-k] = 0
        k += 1
    liste[-k] += 1
    return None


def appartient(x,l):
    n=len(l)
    k=0
    while (k<n) and (l[k] != x):
        k += 1
    return(k!=n)

def jonglable_NB(l,NB):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NBbis = somme/long
    if (NBbis != int(NBbis)) or (NBbis != NB) :
        return(False)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n)

#Rustines

def suivant_rustine(liste,max):
    n = len(liste)
    if n == 0:
        return [0]
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1):
        liste[-k] += 1
        return(liste)
    else:
        return ([0]*(n+1))
  
def lancer_sur(etats,h):
    etats = etats[1:] + [False]
    if h != 0:
        etats[h-1] = True
    return (etats)

def lancer_verifie(etats,h):
    if h == 0:
        if etats[0]:
            return False,False
        else:
            return True,(etats[1:] + [False])
    if (not etats[0]):
        return False,False
    if etats[h]:
        return False,False
    else:
        etats = etats[1:] + [False]
        etats[h-1] = True
        return (True,etats)



def forcing1(l1,l2,max):
    etats = [False for k in range (0,max+1)]
    n1,n2 = len(l1),len(l2)
    for k in range (0,n1):
        etats = lancer_sur(etats,l1[k])
    
    taille_max = 6
    rustine = []
    lr = 0
    bool = False
    while (lr <= taille_max) and (not bool):
        bool = True
        etats_test = etats[:]
        liste = (rustine[:] + l2[:])[:]
        n3 = len(liste)
        k=0
        
        while bool and (k < n3):
            if k == len(rustine):
                apresrus = etats_test[:]
            bool,etats_test = lancer_verifie(etats_test,liste[k])
            k += 1
        if not bool:
            rustine = suivant_rustine(rustine[:],max)[:]
            lr = len(rustine)
    
    return(bool,rustine)

#rustines entre etats (nouveau)

def genere_etat(mot,NB,Hmax):
    etats = [False for k in range(0,Hmax+1)]
    while sum(etats) != NB:
        for k in range(0,len(mot)):
            etats = lancer_sur(etats,mot[k])
    return(etats)




def indiceEmboitage(etat1,etat2):
    etattest = etat1[:] + [False for k in etat1]
    k = 0
    ok = False
    while not ok:
        indices = []
        i=0
        while (i< len(etat1)) and ((etattest[k+i] - etat2[i]) <= 0):
            if (etattest[k+i] - etat2[i]) != 0:
                indices += [k+i]
            i += 1
        ok = (i == len(etat1))
        k += 1
    return(k-1,indices)


def rustine_etats(etat1,etat2):
    i,indices = indiceEmboitage(etat1,etat2)
    rustine = []
    ii=0
    for k in range (0,i):
        if not etat1[k]:
            rustine += [0]
        else:
            rustine += [indices[ii] - k]
            ii += 1
    return rustine



#Graphes

def graphaux(jonglables,graphe,NB): #Ne convient pas
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables.pop()
        n = len(graphe)
        i = 0
        while i<n :
            candidat = graphe[i][0]
            if jonglable_NB(mot + candidat,NB):
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux(jonglables,graphe,NB))

def graph(p,NB,Hmax):
    return(graphaux(gene_pour_graphe(p,NB,Hmax),[],NB))

def graph_emonde(graph): #On ne conserve qu'un element par classe d'equiv (ie l'etat pour graph_bis ci-après)
    return([k[0] for k in graph])

def echainnement_dans_graph(graph,Hmax):   #Les enchainnement ne sont pas toujours corrects
    echainnement = [[] for k in range(0,len(graph))]
    for k in range (0, len(graph)):
        for i in range (0, len(graph)):
            succes,rustine = forcing1(graph[k] * 10, graph[i] * 10,Hmax)
            if succes:
                echainnement[k] += [rustine]
            else:
                print("echainnement_dans_graph : rustine introuvable")
                echainnement[k] += [[]]
    return(echainnement)








def graphaux_bis(jonglables,graphe,NB,Hmax): #se basant sur les états
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables.pop()
        n = len(graphe)
        i = 0
        etat = genere_etat(mot,NB,Hmax)
        while i<n :
            candidat = graphe[i][0] #Graph de la forme : [etat,figure1,figure2,figure3,...],[etat',figure1',figure2',figure3',...],...
            if candidat == etat:
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[etat,mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux_bis(jonglables,graphe,NB,Hmax))


def echainnement_dans_graph_bis(graph,Hmax): # /!\ UTILISE UN GRAPHE EMONDE
    echainnement = [[] for k in range(0,len(graph))]
    for k in range (0, len(graph)):
        for i in range (0, len(graph)):
            echainnement[k] += [rustine_etats(graph[k],graph[i])]
    return(echainnement)

def graph_bis(p,NB,Hmax):
    graphe = graphaux_bis(gene_pour_graphe(p,NB,Hmax),[],NB,Hmax)
    return([k[1:] for k in graphe],echainnement_dans_graph_bis(graph_emonde(graphe),Hmax)) #pour des soucis de lisibilité, on dissocie les classes d'equiv (desquelles on a enlevé les etats) des transition
    
#PS: Caml est un language de COMPOSITION... :P

def graph_bis_numero(liste,NB,Hmax):
    graphe = graphaux_bis(liste,[],NB,Hmax)
    return([k[1:] for k in graphe],echainnement_dans_graph_bis(graph_emonde(graphe),Hmax))


def graphEnListe(graphe):
    liste = []
    for k in range (0,len(graphe)):
        for mot in graphe[k]:
            liste += [ [mot,k] ]
    return liste





#Determination de la difficulté, et tri des elts en fonction de cette dernière

def difficulte(mot,NB,Hmax,longMax):
    n = len(mot)
    
    #Longueur de la periode
    difPerio = n/longMax
    
    #Ecart-type ; je m'arrange pour qu'il soit compris entre 0 et 1
    moyenne = NB /Hmax #Ici, j'ai pris "max", mais j'aurais aussi pu prendre le lancer le plus haut présent dans la liste
    var = 0
    for k in mot:
        var += (k/Hmax - moyenne)*(k/Hmax - moyenne) * 4 #En fait, c'est la variance sans multiplier par la proba( = 1/n)
    difEC = np.sqrt(var / n)
    
    #Différence de hauteur de lancers deux à deux adjacents
    somme = 0
    for k in range (0,n):
        somme += abs(mot[k-1] - mot[k])/Hmax
    difAdj = somme/n
    
    res = difPerio*(1/3) + difEC*(1/3) + difAdj*(1/3) #Il faudra surement choisir des coefficients plus réflechis que ça pour les différentes difficultés
    
    return res


def tri_quadra(liste,NB,Hmax,longMax): #insertion
    liste = [ k + [ difficulte(k[0],NB,Hmax,longMax) ] for k in liste ]
    res = [ [0,0,2] ] #2 est une difficulté absurde pour eviter les depassements de liste, on elevera cet elt à la fin
    for k in liste:
        i=0
        while k[2] > res[i][2]:
            i+=1
        res = res[:i] + [k] + res[i:]
    return res[:-1]

def tri_lin(liste,NB,Hmax,longMax): #dichoto #Et oui, un tri en nlog(n) est linéaire, tout le monde sait ça <(^.^)>
    liste = [ k + [ difficulte(k[0],NB,Hmax,longMax) ] for k in liste ]
    res = [ [0,0,-1],[0,0,2] ] #Idem que précédemment
    for k in liste:
        n = len(res)
        u = n-1
        l = 0
        i = (u+l) // 2
        while ( k[2] > res[i+1][2] ) or (k[2] <= res[i][2]):
            if k[2] > res[i+1][2]:
                l = i
            else:
                u = i
            i = (u+l)//2
        res = res[:i+1] + [k] + res[i+1:]
    return(res[1:-1])
            

def partitionDifficulte(liste,nombre): # utilise une liste de tri_lin/quadra    nombre = nb de groupe de difficulté (le dernier sera le plus gros si ça ne tombe pas juste)
    n = len(liste)
    NpG = n // nombre
    res = []
    indice =0
    
    for k in range(0,nombre -1): #On gere le dernier à part
        resaux = []
        for i in range(0,NpG):
            resaux += [liste[indice][:-1]]
            indice += 1
        res += [resaux]
        
    resaux =[]
    while indice < n:
        resaux += [liste[indice][:-1]]
        indice += 1
        
    return (res + [resaux])

#On a donc des groupes de difficultés dont les elts sont des listes de 2 elts : le premier qui est le mot jonglable, et le second qui est son etat
#Pour les echainner, on utilise donc la liste des transitions donnée par graph_bis



#Génération de numéros

def numeroBogo(PerioMin,PerioMax,NB,Hmax,lancers): # "Stupide" (ie sans logique),bien pour les numéros courts (< 100 lancers)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    
    cascade = [NB]*PerioMax #On commence et on fini par elle
    
    listeFigures = graphEnListe(graphe)
    #gpFigure = partitionDifficulte(tri_lin(listeFigure,NB,Hmax,PerioMax),5) #5 à parametrer en fct de "lancers"
    #En fait pas besoin de difficulté ici
    numero = cascade[:]
    etat = 0
    
    while len(numero) < lancers:
        choix = rng.choice(listeFigures)
        numero += enchainnements[etat][(choix[1])]
        etat = choix[1]
        iteration = rng.choice(range(2,5)) #à éditer
        for k in range(0,iteration):
            numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    return numero

def fonctionDifficulte(x,a,b): #a entre 1 et 10 (entre 1 et 5 pour les fct), b entre 1 et 2 #On veut a ou b augmente => difficulté augmente #a est la difficulté "principale", et b la "secondaire"
    if a < 6:
        return (1 - x**((a/5)/(3-b))) #Bidouillage sur les facteurs de a et b pour respecter les condictions ci-dessus
    else:
        return (x**(((11-a)/5)/b)) #idem

def fonctionDifficulteBis(x,a):
    if a < 6:
        return (1 - x**(a/5)) 
    else:
        return (x**((11-a)/5))


def numeroLogiqueLong(PerioMin,PerioMax,NB,Hmax,lancers): #Bien pour les numéro longs (>500-1000)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    
    cascade = [NB]*PerioMax #On commence et on fini par elle
    
    ngp = 5 #correspond au nombre de groupes de difficultés, il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    gpFigure = partitionDifficulte(tri_lin(listeFigures,NB,Hmax,PerioMax),ngp)
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5
    
    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait, er on considère que l'on ne fait que figures de PerioMax avec des rustine de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1) #Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)
    
    if NbFigure < 50:
        bMax = 5
    elif NbFigure < 200:
        bMax = 10
    elif NbFigure < 500:
        bMax = 15
    else:
        bMax = 20
    
    aMax = NbFigure // bMax
    
    for a in range (0,aMax+1):
        for b in range (0,bMax+1):
            choixDifficulte = fonctionDifficulte(rng.random(),1 + ( (a/aMax)*4 ), 1 + ( b/bMax ) )
            k=1
            enCours = True
            while enCours:
                if k / ngp >= choixDifficulte:
                    enCours = False
                    k -= 1 #Pour correspondre à l'indice de la liste
                else:
                    k+=1
            choix = rng.choice(gpFigure[k])
            numero += enchainnements[etat][(choix[1])]
            etat = choix[1]
            iteration = rng.choice(range(iterationMin,iterationMax+1))
            for k in range(0,iteration):
                    numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    return numero

def numeroLogiqueCourt(PerioMin,PerioMax,NB,Hmax,lancers): #Bien pour les numéro intermédiaires (On a enlevé b)
    listeFigures = []
    for p in range (PerioMin,PerioMax+1):
        listeFigures += gene_pour_graphe(p,NB,Hmax)
    graphe,enchainnements = graph_bis_numero(listeFigures,NB,Hmax)
    
    cascade = [NB]*PerioMax #On commence et on fini par elle
    
    ngp = 5 #correspond au nombre de groupes de difficultés, il faudra plus tard la parametrer en fct de "lancers"
    listeFigures = graphEnListe(graphe)
    gpFigure = partitionDifficulte(tri_lin(listeFigures,NB,Hmax,PerioMax),ngp)
    numero = cascade[:]
    etat = 0
    iterationMin = 2 #à éditer
    iterationMax = 5
    
    #On va utiliser des moyennes pour estimer le nombre de lancer qu'on a fait, er on considère que l'on ne fait que figures de PerioMax avec des rustine de taille PerioMax
    moyLancerParFig  = ( (iterationMax+iterationMin)/2 ) * (PerioMax +1) #Le +1 correspond à la rustine
    NbFigure = lancers // int(moyLancerParFig)
    
    aMax = NbFigure
    
    for a in range (0,aMax+1):
        choixDifficulte = fonctionDifficulteBis(rng.random(),1 + ( (a/aMax)*4 ))
        k=1
        enCours = True
        while enCours:
            if k / ngp >= choixDifficulte:
                enCours = False
                k -= 1 #Pour correspondre à l'indice de la liste
            else:
                k+=1
        choix = rng.choice(gpFigure[k])
        numero += enchainnements[etat][(choix[1])]
        etat = choix[1]
        iteration = rng.choice(range(iterationMin,iterationMax+1))
        for k in range(0,iteration):
                numero += choix[0]
        
    numero += enchainnements[etat][0] + cascade[:]
    
    return numero








def TempsEnLancers(NbSecondes):
    return(NbSecondes*3)





#Algorithmes de debugage

def listeEnNombre(liste):
    res =""
    for k in liste:
        res+= str(k)
    return int(res)

def nombreEnListe(nombre):
    return([int(k) for k in list(str(nombre))])

def indicePlantage(numero):
    etat = [True,True,True] + [ False for k in range(0,10)]
    i=0
    for k in numero:
        bool,etat = lancer_verifie(etat,k)
        if not bool:
            return i
        i+=1
    return True


































