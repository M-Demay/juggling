## Quelques outils comme d'habitude

def suivant_rustine(liste,max):
    n = len(liste)
    if n == 0:
        return [0]
    # On recherche le premier element inferieur au max
    k = 1
    while (k != (n+1)) and (liste[-k] == max):
        liste[-k] = 0
        k += 1
    if k != (n+1): # Dans ce cas c'est la condition liste[-k]==max qui n'est pas remplie : on augmente donc cette valeur de 1
        liste[-k] += 1
        return(liste)
    else: # Dans ce cas tout le monde est egal au max ; on teste donc les rustines de longueur superieure.
        return ([0]*(n+1))


def lancer_verifie(etats,h): # Renvoie un booleen pour dire si on peut faire le lancer h dans l'etat etats, et l'etat suivant le cas echeant.
    if h == 0:
        if etats[0]:
            return False,False
        else:
            return True,(etats[1:] + [False])
    #A partir d'ici le lancer n'est pas un 0
    if (not etats[0]): #Cas ou on n'a pas de balle a lancer
        return False,False
    if etats[h]: # Cas ou l'"endroit d'arrivee" est deja occupe
        return False,False
    else: # Si on est arrive ici c'est que tout va bien, on peut faire le lancer.
        etats = etats[1:] + [False]
        etats[h-1] = True
        return (True,etats)

def graphaux(jonglables,graphe,NB): #C'est un nez tchèque
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables.pop()
        n = len(graphe)
        i = 0
        while i<n :
            candidat = graphe[i][0]
            if jonglable_NB(mot + candidat,NB):
                graphe[i] += [mot]
                i = n
            i += 1
        if i == n:
            graphe += [[mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux(jonglables,graphe,NB))

# Ce sont des classes d'equivalence par la relation : "se jongle a la suite de" !
# Probleme avec 3 balles, periode 3 : on recupere dans la meme sous-liste les mots [9, 0, 0], [8, 0, 1], [7, 2, 0], [7, 1, 1], qui se jonglent effectivement deux a deux mais pas tous a la suite. De plus j'ai remarque qu'ils ne "s'excitaient" pas de la meme facon : etats differrents ? De plus les programmes pour trouver les rustines(forcing1,forcing2) sont faux.

def jonglable_NB(l,NB):
    # Condition necessaire
    somme,long = sum(l),len(l)
    NBbis = somme/long
    if (NBbis != int(NBbis)) or (NBbis != NB) :
        return(False)
    
    # Condition necessaire et suffisante
    n = len(l)
    s = []
    for k in range(0,n):
        s += [(l[k] + k)%n]
    k=0
    suite=[k for k in range(0,n)]
    while (k<n) and (appartient(s[k],suite)):
        suite[s[k]] = 0.5  #Pour Python, True=1 et False=0 ... #Lol, #DesBarres,tyr #JeHaisLaVie
        # Rectification : pour Python : False = 0 et tout nombre non nul est vrai.
        k+=1
    return (k==n)



## Methode de Mathis pour determiner les rustines
# Deja on va se focaliser sur les rustines a partir de la cascade. En effet le seul etat connu sans rien faire est celui de la cascade. Si ensuite on veut une rustine entre deux excites, on demandera la rustine entre la cascade et le premier excite afin d'obtenir correctement l'etat de fin du premier excite, et on appliquera le meme algotithme entre les deux excites qu'entre la cascade et le premier.
# On va donc refaire une fonction pour remplacer graphaux.

def etatdarrivee(etat,rustine):
    #Donne l'etat d'arrivee a partir de etat en faisant les lancers preconises par la rustine, si possible. Sinon, renvoie None.
    etataux = etat[:]
    bool = True
    k = 0 # k sert a parcourir les lancers de rustine
    n = len(rustine)
    while bool and k < n : # Le strict permet que, meme lorsque rustine == [], ca marche ! Car k = 0 donc la condition k<0 est fausse.
        bool,etataux = lancer_verifie(etataux,rustine[k]) # Probleme : depassement d'indice ici... Sans doute une histoire d'etats qui ne contiennent pas assez de False.
        k +=1
    if bool :
        return(etataux)
    else :
        return(None)

def rustine_entree_excite(mot,max): # Donne la bonne rustine pour arriver a mot a partir de la cascade. Se termine bien car il existe toujours une rustine. A prouver ?
    NB = int(sum(mot)/len(mot)) # Nombre de balles. Il faut mettre un int devant car sinon on obtient un flottant, non multipliable par une liste.
    etat_init = (NB * [True]) + ((max - NB + 1) * [False]) # Etat de la cascade, avec la bonne hauteur
    etat_avant , etat_apres = [],[] # Ce sont les etats au debut et a la fin de la figure
    bool = False # Indique si on a trouve la bonne rustine
    rustine = []
    
    #1re iteration, necessaire car sans elle on ne testerait pas le cas de la rustine [].
    etat_avant = etatdarrivee(etat_init , rustine)
    etat_apres = etatdarrivee(etat_avant,mot)
    bool = (etat_avant == etat_apres)
    while not bool :
        rustine = suivant_rustine(rustine,max)
        etat_avant = etatdarrivee(etat_init , rustine)
        if etat_avant != None :
            etat_apres = etatdarrivee(etat_avant,mot)
            bool = (etat_avant == etat_apres)
    return(rustine,etat_avant)

def rustines_et_etat(mot,max): # Operation inverse : comment sortir de l'etat excite pour revenir a celui de la cascade ?
        NB = int(sum(mot)/len(mot)) # Nombre de balles
        rus_ent,etat_init = rustine_entree_excite(mot,max) # C'est l'etat que mot conserve.
        etatvoulu = NB*[True] + (max - NB + 1) * [False] # C'est l'etat de la cascade.
        bool = False
        rus_sor = []
        
        #1re etape
        bool = (etatdarrivee(etat_init,rus_sor) == etatvoulu)

        while not bool :
            rus_sor = suivant_rustine(rus_sor,max)
            bool = (etatdarrivee(etat_init,rus_sor) == etatvoulu)
        return(rus_ent,rus_sor,etat_init) # On a obtenu l'etat excite ; on le retourne quand meme, on sait jamais.

def rustine_entre_etats(etat1,etat2,NB,max):
    rustine = []
    bool = False
    bool = ( etatdarrivee(etat1,rustine) == etat2 )
    
    while not bool :
        rustine = suivant_rustine(rustine,max)
        bool = ( etatdarrivee(etat1,rustine) == etat2 )
    
    return(rustine)
    
    
def graphaux2(jonglables,graphe,NB,max):
    if jonglables ==[]:
        return graphe
    else:
        # Attention ici on detruit l'argument ! Faire une deepcopy avant d'utiliser cette fonction!!!
        mot = jonglables.pop()
        rus_ent,rus_sor,etat_mot= rustines_et_etat(mot,max)
        
        n = len(graphe)
        i = 0
        while i<n :
            etat_candidat = graphe[i][2]
            
            if etat_mot == etat_candidat :
                graphe[i] += [mot]
                i = n # Justifie le while et non le for
            i += 1
        if i == n:
            graphe += [[rus_ent,rus_sor,etat_mot,mot]]
            # Donc ca marche bien meme si graphe est la liste vide !
        return(graphaux2(jonglables,graphe,NB,max))
# Ainsi a ce moment graphe est de la forme [ [transition_d'entree1,transition_de_sortie1,etat1,mot1,mot2,...] , ... ]

def graphaux3(graphe,NB,max): # Renvoie la liste des figures triees par etat, ainsi que les transitions d'un etat a l'autre, et ce pour tous les etats : chacune contient la transition D'ELLE-MEME VERS L'ETAT SUIVANT. Graphe est la liste des jonglables modifiee par graphaux2
    for liste in graphe :
        #Nettoyage
        del liste[0]
        del liste[0]
    for liste in graphe : #On ajoute les transitions. Seulement, comme on a supprime la transition de la cascade, on fait un calcul inutile par element de graphe. C'est quand meme surement plus rentable, puisque sinon elle aurait ete a la mauvaise place... Donc on aurait pu la garder en faisant un test d'egalite couteux a chaque nouvelle rustine ajoutee ; ou alors placer toujours la cascade au debut lors de la generation meme, ca c'est bien.
        liste += [False]
        for k in range(len(graphe)) :
            liste += [rustine_entre_etats(liste[0],graphe[k][0],NB,max)]
    return(graphe) # Bon, ca marche pas/ AAAH ca marche!!
    
    #Reste un truc : comment distinguer une figure d'une rustine ? Mettre un marqueur ou alors peut-etre que la moyenne d'une rustine n'est jamais egale au nombre de balles : exemple avec 3 balles, periode 3 : il y a des rustines comme [1,4] qui ont une moyenne non entiere, ou [5,7] qui a pour moyenne 6 et non 3.
    #Bon bah c'est faux : avec le meme exemple on a une rustine [5,1] de moyenne 3. On met donc un False en plein milieu de chaque liste parce que les booleens c'est bien. On aurait aussi pu mettre du texte mais au moment de l'interpretation, ce sera plus efficace de chercher un False qu'une chaine de caracteres.
    # On va vers une table exhaustive, avec toutes les permutations circulaires qui donnent d'autres figures, encore que le jongleur sait quand meme faire ca. Mais on peut aussi melanger les periodes, et ca c'est super ! Parce que jusqu'ici on n'enchainait pas 423 4 51 2 et ca c'est quand meme dommage.²
    
##Note : ce qu'on a fait ne depend pas de la periode, il semblerait. Ceci ouvre tout un tas de possibilites !

##Fin
##Bilan : nouvelle methode pour faire le graphe.
#Exemple a trois balles, periode 3 : [9,0,0] s'excite avec [5,7] ; [8,0,1] s'excite avec [4,6]. Ainsi ces deux excites ne sont pas dans le meme etat. Cependant ils peuvent s'enchainer directement, c'est-a-dire que le mot [9,0,0,8,0,1] est jonglable, et le tout s'excite avec [5,6]. Ainsi pour rassembler les figures dans le graphe on ne fera pas par "enchainabilite deux par deux" mais par meme etat. Ensuite, pour les rustines entre etats, on determinera toutes les rustines avec la cascade et on concatenera deux rustines : une de sortie du premier, une d'entree du second, pour obtenir la rustine finale entre les deux. Ou alors, pour esperer trouver une rustine plus courte, on cherchera a passer d'un etat a l'autre.

def test():
    jonglables = genere_echanges_adj_ite(3,3)
    graphe = graphaux2(jonglables,[],3,9)
    return(graphaux3(graphe,3,9))

def veritesurgraphaux(graphe,NB):
    res = []
    for bloc in graphe :
        for j in range(len(bloc)):
            for k in range(j+1,len(bloc)):
                res += [jonglable_NB(bloc[j]+bloc[k],NB)]
    return(app_fal(res))

def app_fal(liste):
    if liste == [] :
        return(False)
    else :
        return( (not(liste[0])) or (app_fal(liste[1:])) )
    
def veritesurgraphaux_bis(b,p) :
    jonglables = genere_echanges_adj_ite(b,p)
    graphe = graphaux(jonglables,[],b)
    return(veritesurgraphaux(graphe,b)) # Renvoie True si j'ai raison
    #Bon bah soit il plante soit il renvoie False...






