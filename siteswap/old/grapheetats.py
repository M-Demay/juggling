def ens_etats(b,h):
    # Renvoie la liste des etats pour b balles et une hauteur de h
    if b == h:
        return([[True]*b])
    if b == 0 :
        return([[False]*h])
    else:
        JSP1 = ens_etats(b, h-1)
        JSP2 = ens_etats(b-1,h-1)
        return [[False] + k  for k in JSP1] + [[True] + k for k in JSP2]

def lancers_possibles(etat) :
    # Renvoie la liste des lancers possibles a partir d'un etat
    if (not etat[0]) :
        return([0])
    else :
        res = []
        etatbis = etat[1:] + [False]
        for k in range (0,len(etatbis)):
            if not etatbis[k]:
                res += [k+1]
        return(res)

def etat_suivant(etat,lancer) :
    # Renvoie l'etat dans lequel on se trouve apres avoir effectue un lancer en etant dans un etat
    res = etat[1:]
    letat = len(etat)
    if lancer == 0 :
        res = res + [False]
    else :
        etat[lancer] = True
    return(res)


def position(etat,graph):
    # On sait que etat se trouve dans graph ; on renvoie sa position dans graph (indice dans la liste donc a partir de 0)
    if graph[0][0] == etat :
        return(0)
    else :
        return(1 + position(etat, graph[1:]))
    


def graphecomplet(b,h):
    # Renvoie un graphe de la forme [ [ etat, [lancer1, position_de_l'etat_d'arrivee1], [lancer2, ...] , ... ] , [autre_etat,...] ]
    # Plein de print pour le debug ; il ne veut pas toujours generer correctement etatprime. Exemple d'execution avec graphecomplet(1,3)
    graph = [ [k] for k in ens_etats(b,h) ]
    print('graph',graph)
    etat, etatprime = [] , []
    for k in graph :
        etat = k[0]
        for j in lancers_possibles(etat):
            etatprime = etat_suivant(etat,j)
            print('etatprime',etatprime)
            pos = position(etatprime,graph)
            print('pos', pos)
            k += [[j,pos]]
    return(graph)

## Test
def patate():
    etat = [False, True, False, False]
    graph = [ [k] for k in ens_etats(1,4) ]
    print(graph)
    print(position(etat,graph))